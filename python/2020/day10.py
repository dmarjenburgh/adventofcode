import python.common as common
from collections import Counter
from itertools import chain, combinations


def part_1(data):
    nums = [int(l) for l in data.splitlines()]
    nums = sorted(nums)
    diffs = []
    last = 0
    for i in range(len(nums)):
        d = nums[i] - last
        last = nums[i]
        diffs.append(d)
    diffs.append(3)
    freqs = Counter(diffs)
    return freqs[1] * freqs[3]


def valid_chain(schain):
    if len(schain) < 2:
        return False
    for i in range(len(schain) - 1):
        if schain[i + 1] - schain[i] > 3:
            return False
    return True


def create_subchains(nums):
    f, l = nums[0], nums[-1]
    c = nums[1:-1]
    return [[f] + list(x) + [l] for x in chain.from_iterable(combinations(c, r) for r in range(len(c) + 1))]


def count_subchains(ch):
    n = len(ch)
    for i in range(1, n - 1):
        if ch[i + 1] - ch[i - 1] > 3:
            return count_subchains(ch[:i + 1]) * count_subchains(ch[i:])
    subchains = create_subchains(ch)
    return len([sc for sc in subchains if valid_chain(sc)])


def part_2(data):
    nums = [int(l) for l in data.splitlines()]
    nums = [0] + sorted(nums)
    nums.append(nums[-1] + 3)
    return count_subchains(nums)


print(part_1(common.load_input(2020, 10)))
print(part_2(common.load_input(2020, 10)))
