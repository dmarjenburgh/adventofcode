import itertools
import re
import python.common as common
from operator import mul
import functools


def parse_input(data):
    parts = data.split("\n\n")
    tiles = {}
    for part in parts:
        if not part:
            continue
        lines = part.splitlines()
        tile_nr = int(re.findall(r"\d+", lines[0])[0])
        tiles[tile_nr] = lines[1:]
    return tiles


def rotate(tile):
    return [''.join(reversed([l[i] for l in tile])) for i in range(len(tile))]


def flip(tile):
    return [''.join(reversed(s)) for s in tile]


def transform_image(tile, xform):
    r, flipped = xform
    tile = flip(tile) if flipped else tile
    for r in range(r):
        tile = rotate(tile)
    return tile


def try_fit(n, tiles, xform=(0, False)):
    """Tries to fit all tiles on top of tile n. Returns list of matching tiles with orientation"""
    tile = transform_image(tiles[n], xform)
    matches = []
    for i, t in tiles.items():
        if i != n:
            for flipped in [False, True]:
                t = flip(t) if flipped else t
                for r in range(4):
                    if tile[0] == t[9]:
                        matches.append((i, (r, flipped)))
                    t = rotate(t)
    return matches


def categorize_tiles(tiles):
    """Identify corner, edge and center tiles"""
    corner_tiles = []
    edge_tiles = []
    center_tiles = []
    for n, _ in tiles.items():
        count = 0
        for r in range(4):
            if not try_fit(n, tiles, (r, False)):
                count += 1
        if count == 2:
            corner_tiles.append(n)
        elif count == 1:
            edge_tiles.append(n)
        elif count == 0:
            center_tiles.append(n)
        else:
            raise Exception(f"Impossible number of nonmatching edges: {count}")
    return corner_tiles, edge_tiles, center_tiles


def compose_xform(x1, x2):
    r1, f1 = x1
    r2, f2 = x2
    return (r1 + r2) % 4, f1 ^ f2


def orient_corner(tile_n, tiles):
    rs = []
    for r in range(4):
        if not try_fit(tile_n, tiles, (r, False)):
            rs.append(r)
    return 3 if 3 in rs else min(*rs), False


def remove_borders(tile):
    return [row[1:-1] for row in tile[1:-1]]


def print_image(image):
    for line in image:
        print(line)


def assemble_image(jigsaw_sol, tiles):
    image = []
    for row in jigsaw_sol:
        img_row = []
        for tile_n, xform in row:
            bordered_tile = transform_image(tiles[tile_n], xform)
            tile = remove_borders(bordered_tile)
            img_row.append(tile)
        image.append(img_row)
    parts = [list(itertools.starmap(lambda *a: ''.join(a), zip(*f))) for f in image]
    return [part for sublist in parts for part in sublist]


monster_image = sm = ['                  # ',
                      '#    ##    ##    ###',
                      ' #  #  #  #  #  #   ']
monster_positions = set((y, x) for y, row in enumerate(monster_image) for x, ch in enumerate(row) if ch == '#')


def monster_at(image, pos):
    row, col = pos
    actual_coords = [(r + row, c + col) for r, c in monster_positions]
    for r, c in actual_coords:
        if image[r][c] != '#':
            return None
    return actual_coords


def find_seamonsters(image):
    w, h = len(image[0]), len(image)
    max_x = w - 20
    max_y = h - 3

    monster_coords = set()

    for row in range(max_y):
        for col in range(max_x):
            mc = monster_at(image, (row, col))
            if mc:
                monster_coords.update(mc)
    return monster_coords


def solve(data):
    tiles = parse_input(data)
    corner_tiles, edge_tiles, center_tiles = categorize_tiles(tiles)

    print(f"Part 1: {functools.reduce(mul, corner_tiles)}")

    # Pick first piece for top left corner (it's arbitrary anyway)
    tl_x = orient_corner(corner_tiles[0], tiles)
    tiles_to_place = set(tiles.keys())
    tiles_to_place.remove(corner_tiles[0])
    tiles_placed = [[(corner_tiles[0], tl_x)]]
    while tiles_to_place:
        cur_row = tiles_placed[-1]

        if cur_row:
            cur_tile, cur_x = cur_row[-1]
            m = try_fit(cur_tile, tiles, compose_xform(cur_x, (3, False)))
            if not len(m):
                tiles_placed.append([])
                continue
            assert len(m) == 1, "Matched more than 1 tile"
            t, t_x = m[0]
            cur_row.append((t, compose_xform(t_x, (1, False))))
            tiles_to_place.remove(t)
        else:
            above_tile, at_x = tiles_placed[-2][0]
            m = try_fit(above_tile, tiles, compose_xform(at_x, (2, False)))
            assert len(m) == 1, "Matched more than 1 tile"
            t, t_x = m[0]
            cur_row.append((t, compose_xform(t_x, (2, False))))
            tiles_to_place.remove(t)

    assembled_image = assemble_image(tiles_placed, tiles)
    print(f"Assembled image size: {len(assembled_image[0])} x {len(assembled_image)}")
    # print_image(assembled_image)

    for flipped in [False, True]:
        for r in range(4):
            img = transform_image(assembled_image, (r, flipped))
            monster_parts = find_seamonsters(img)
            image_parts = set((y, x) for y, row in enumerate(img) for x, col in enumerate(row) if col == '#')
            print("Finding seamonsters under transformation:",
                  (r, flipped),
                  f"{len(monster_parts)} parts found" if monster_parts else "No monster parts")
            if monster_parts:
                print(f"Part 2: {len(image_parts.difference(monster_parts))}")


if __name__ == '__main__':
    solve(common.load_input(2020, 20))
