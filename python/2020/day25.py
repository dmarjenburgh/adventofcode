import python.common as common
from functools import reduce
from collections import Counter


def parse_input(data):
    return [int(x) for x in data.splitlines()]


def find_loop_size(pkey, subject_number):
    n = 0
    v = 1
    while v != pkey:
        v = v * subject_number % 20201227
        n += 1
    return n


def do_transform(loop_size, subject_number):
    v = 1
    for _ in range(loop_size):
        v = v * subject_number % 20201227
    return v


def solve(data):
    card_pkey, door_pkey = parse_input(data)
    card_loop_size = find_loop_size(card_pkey, 7)
    door_loop_size = find_loop_size(door_pkey, 7)
    print(do_transform(card_loop_size, door_pkey))
    print(do_transform(door_loop_size, card_pkey))


if __name__ == '__main__':
    solve(common.load_input(2020, 25))
    # solve("5764801\n17807724")
