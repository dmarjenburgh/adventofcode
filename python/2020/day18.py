import re

import python.common as common


# WARNING! Super ugly code ahead

def parse_input(data):
    return data.splitlines()


def read_token(expr: str):
    lvl = 0
    se = []
    for i, ch in enumerate(expr):
        if ch == '(':
            if lvl > 0:
                se.append(ch)
            lvl += 1
            continue
        elif ch == ')':
            lvl -= 1
            if lvl == 0:
                return ''.join(se), expr[i + 1:]
        if lvl > 0:
            se.append(ch)
            continue

        if ch == ' ':
            continue
        elif ch.isdigit():
            return re.match(r"\d+", expr[i:])[0], re.split(r"\d+", expr, 1)[1]
        elif ch in '+*':
            return ch, expr[i + 1:]


def tokenize(expr: str):
    tokens = []
    while len(expr):
        token, expr = read_token(expr)
        tokens.append(token)
    return tokens


def eval_expr(expr) -> int:
    tokens = tokenize(expr)
    while True:
        if len(tokens) == 1:
            t = tokens[0]
            if t.isnumeric():
                return int(t)
            else:
                return eval_expr(t)
        elif len(tokens) > 1:
            lhs, op, rhs = tokens[:3]
            if op == '+':
                v = eval_expr(lhs) + eval_expr(rhs)
            else:
                v = eval_expr(lhs) * eval_expr(rhs)
            tokens = [str(v)] + tokens[3:]


def insert_parens(expr):
    tokens = tokenize(expr)
    acc = []
    close = False
    b = []
    for t in tokens:
        if t == '+':
            last = acc.pop()
            b = ['(', last, t]
            close = True
        else:
            if len(t) > 1 and not t.isnumeric():
                t = f"({insert_parens(t)})"
            if close:
                b.extend([t, ')'])
                acc.append(''.join(b))
                b = []
                close = False
            else:
                acc.append(t)
    return ' '.join(acc)


def solve(data):
    print(f"Part 1: {sum(eval_expr(e) for e in parse_input(data))}")
    print(f"Part 2: {sum(eval_expr(insert_parens(e)) for e in parse_input(data))}")


if __name__ == '__main__':
    solve(common.load_input(2020, 18))
