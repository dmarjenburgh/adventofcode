import python.common as common

# Deque was not fast enough


def parse_input(data):
    return [int(x) for x in data]


def move(cur, d, max_cup=9):
    a1 = d[cur]
    a2 = d[a1]
    a3 = d[a2]
    a4 = d[a3]
    dest = cur - 1
    if dest == 0:
        dest = max_cup
    while dest in [a1, a2, a3]:
        dest -= 1
        if dest == 0:
            dest = max_cup
    old_dest = d[dest]
    d[dest] = a1
    d[a3] = old_dest
    d[cur] = a4
    return a4


def build_circ_ll(nums):
    d = dict(zip(nums, nums[1:]))
    d[nums[-1]] = nums[0]
    return d


def solve(data):
    nums = parse_input(data)
    d = build_circ_ll(nums)
    cur = nums[0]
    for _ in range(100):
        cur = move(cur, d, max_cup=9)

    xs = []
    cur = 1
    while len(xs) < len(d):
        cur = d[cur]
        xs.append(str(cur))
    print(''.join(xs[:-1]))

    d = build_circ_ll(nums + list(range(1000001))[10:])
    cur = nums[0]
    for n in range(10000000):
        cur = move(cur, d, max_cup=1000000)

    x1 = d[1]
    x2 = d[x1]

    print(x1 * x2)


if __name__ == '__main__':
    solve(common.load_input(2020, 23))
    # solve("389125467")
