import python.common as common
import re

data = common.load_input(2020, 4)

passports = [s.replace('\n', ' ') for s in data.split("\n\n")]


def year_validator(low, hi):
    return lambda x: re.match(r"^\d{4}$", x) and low <= int(x) <= hi


def valid_height(s):
    m = re.match(r"^(\d+)(cm|in)$", s)
    if m:
        v, u = m.groups()
        if u == 'in':
            return 59 <= int(v) <= 76
        elif u == 'cm':
            return 150 <= int(v) <= 193
        else:
            return False
    return False


def valid_hcl(s):
    return bool(re.match(r"^#[0-9a-f]{6}$", s))


def valid_ecl(s):
    return s in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']


def valid_pid(s):
    return bool(re.match(r"^\d{9}$", s))


def is_valid(passport, part):
    required_fields = {'byr': year_validator(1920, 2002),
                       'iyr': year_validator(2010, 2020),
                       'eyr': year_validator(2020, 2030),
                       'hgt': valid_height,
                       'hcl': valid_hcl,
                       'ecl': valid_ecl,
                       'pid': valid_pid
                       }
    keyset = set(required_fields.keys())
    kvs = [field.split(':') for field in passport.split(' ')]
    if part == 1:
        parts = set(kv[0] for kv in kvs)
        return set(keyset).issubset(parts)
    else:
        parts = set(kv[0] for kv in kvs)
        return set(keyset).issubset(parts) and all([required_fields[kv[0]](kv[1]) for kv in kvs if kv[0] in keyset])


print(sum([is_valid(p, 1) for p in passports]))
print(sum([is_valid(p, 2) for p in passports]))
