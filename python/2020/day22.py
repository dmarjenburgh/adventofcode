import python.common as common
from collections import deque
from itertools import islice


def parse_input(data):
    p1, p2 = data.split('\n\n')
    return [int(line.strip()) for line in p1.splitlines()[1:]], [int(line.strip()) for line in p2.splitlines()[1:]]


def play_round(d1, d2):
    c1, c2 = d1.popleft(), d2.popleft()
    if c1 > c2:
        d1.extend([c1, c2])
    else:
        d2.extend([c2, c1])


def play_combat(d1, d2):
    while d1 and d2:
        play_round(d1, d2)


def play_recursive_combat(d1, d2):
    game_states = set()
    while d1 and d2:
        h = f"{d1}{d2}"
        if h in game_states:
            return 1

        game_states.add(h)
        c1, c2 = d1.popleft(), d2.popleft()
        if c1 <= len(d1) and c2 <= len(d2):
            winner = play_recursive_combat(deque(islice(d1, c1)), deque(islice(d2, c2)))
            if winner == 1:
                d1.extend([c1, c2])
            else:
                d2.extend([c2, c1])
        elif c1 > c2:
            d1.extend([c1, c2])
        else:
            d2.extend([c2, c1])
    return 1 if d1 else 2


def calculate_score(deck):
    return sum(x * y for x, y in zip(deck, range(len(deck), 0, -1)))


def solve(data):
    p1, p2 = parse_input(data)
    deck1, deck2 = deque(p1), deque(p2)
    play_combat(deck1, deck2)
    print(calculate_score(deck1 or deck2))

    deck1, deck2 = deque(p1), deque(p2)

    play_recursive_combat(deck1, deck2)
    print(calculate_score(deck1 or deck2))


if __name__ == '__main__':
    solve(common.load_input(2020, 22))
