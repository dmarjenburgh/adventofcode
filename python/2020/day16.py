import re
import numpy as np

import python.common as common


def parse_data(data):
    first_paragraph = data.split('\n\n')[0].splitlines()
    rules = []
    for line in first_paragraph:
        name, min1, max1, min2, max2 = re.match(r"^([^:]*): (\d+)-(\d+) or (\d+)-(\d+)", line).groups()
        rules.append([name, int(min1), int(max1), int(min2), int(max2)])
    lines = data.splitlines()
    nearby_tickets_idx = lines.index('nearby tickets:')
    nearby_tickets_lines = lines[nearby_tickets_idx + 1:]
    your_ticket_idx = lines.index("your ticket:")
    your_ticket_line = lines[your_ticket_idx + 1]
    return {
        'rules': rules,
        'nearby_tickets': [[int(x) for x in line.split(',')] for line in nearby_tickets_lines],
        'ticket': [int(x) for x in your_ticket_line.split(',')]
    }


def matched_rules(rules, x):
    mr = set()
    for r in rules:
        if r[1] <= x <= r[2] or r[3] <= x <= r[4]:
            mr.add(r[0])
    return mr


def solve(data):
    puzzle = parse_data(data)
    valid_tickets = []
    errors = []
    rules = puzzle['rules']
    for nt in puzzle['nearby_tickets']:
        has_error = False
        for x in nt:
            if len(matched_rules(rules, x)) == 0:
                has_error = True
                errors.append(x)
        if not has_error:
            valid_tickets.append(nt)

    print(f"Part 1: {sum(errors)}")

    possibilities = [set(r[0] for r in rules) for _ in rules]
    # eliminate possibilities
    for nt in valid_tickets:
        for x, poss in zip(nt, possibilities):
            mr = matched_rules((r for r in rules if r[0] in poss), x)
            poss.intersection_update(mr)
            if len(poss) == 1:
                for p in possibilities:
                    if p != poss:
                        p.difference_update(poss)

    ticket = list(zip([p.pop() for p in possibilities], puzzle['ticket']))
    dep_values = [kv[1] for kv in ticket if kv[0].startswith('departure')]
    print(f"Part 2: {np.prod(dep_values)}")


if __name__ == '__main__':
    solve(common.load_input(2020, 16))
