import python.common as common
import re


# Setting to one can be done with OR
# Setting to zero can be done with AND

# a, X -> (a or 0) and 1 -> a
# a, 0 -> (a or 0) and 0 -> 0
# a, 1 -> (a or 1) and 1 -> 1

def apply_mask(x, m):
    om, am = m
    return (x | om) & am


def parse_line(l):
    if l.startswith('mask'):
        ms = l.split(' = ')[1]
        ormask = int(ms.replace('X', '0'), 2)
        andmask = int(ms.replace('X', '1'), 2)
        return 'mask', (ormask, andmask)
    addr, v = re.match(r"^mem\[(\d+)] = (\d+)", l).groups()
    return 'mem', (int(addr), int(v))


def parse_instructions(data):
    return [parse_line(l) for l in data.splitlines()]


def apply_instr(state, instr):
    op, p = instr
    if op == 'mask':
        state['mask'] = p
    elif op == 'mem':
        addr, v = p
        state['mem'][addr] = apply_mask(v, state['mask'])
    else:
        print(f'Unknown op: {op}')


def apply_mask2(a: str, m: str):
    return ''.join(a[i] if m[i] == '0' else m[i] for i in range(len(m)))


def parse_line2(l):
    if l.startswith('mask'):
        return 'mask',  l.split(' = ')[1]
    addr, v = re.match(r"^mem\[(\d+)] = (\d+)", l).groups()
    return 'mem', ('{:0>36b}'.format(int(addr)), int(v))


def parse_instructions2(data):
    return [parse_line2(l) for l in data.splitlines()]


def apply_instr2(state, instr):
    op, p = instr
    if op == 'mask':
        state['mask'] = p
    elif op == 'mem':
        addr, v = p
        adrs = expand_mask(apply_mask2(addr, state['mask']))
        for a in adrs:
            state['mem'][a] = v
    else:
        print(f'Unknown op: {op}')


def expand_mask(m: str):
    try:
        i = m.index('X')
        return expand_mask(m[:i] + '0' + m[i + 1:]) + expand_mask(m[:i] + '1' + m[i + 1:])
    except ValueError:
        return [int(m, 2)]


def solve(data):
    state = {'mem': {}}
    instructions = parse_instructions(data)
    for instr in instructions:
        apply_instr(state, instr)
    print(f"Part 1: {sum(state['mem'].values())}")

    state = {'mem': {}}
    instructions = parse_instructions2(data)
    for instr in instructions:
        apply_instr2(state, instr)
    print(f"Part 2: {sum(state['mem'].values())}")


if __name__ == '__main__':
    solve(common.load_input(2020, 14))

