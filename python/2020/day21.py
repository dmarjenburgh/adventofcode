import re
import python.common as common


def parse_input(data):
    info = []
    for line in data.splitlines():
        ings, alls = re.match(r"(^.*) \(contains (.*)\)$", line).groups()
        info.append([ings.split(' '), alls.split(', ')])
    return info


def allergen_ingr_map(foods):
    m = {}
    for ings, alg in foods:
        for a in alg:
            if a in m:
                m[a].intersection_update(ings)
            else:
                m[a] = set(ings)
            if len(m[a]) == 1:
                for k in m:
                    if k != a:
                        m[k].difference_update(m[a])
    return m


def solve(data):
    foods = parse_input(data)
    aim = allergen_ingr_map(foods)
    all_ingredients = set(ing for food in foods for ing in food[0])
    ingredients_with_allergens = set.union(*aim.values())
    ingredients_without_allergens = all_ingredients.difference(ingredients_with_allergens)
    print(len([ing for food in foods for ing in food[0] if ing in ingredients_without_allergens]))
    ing_all_list = sorted([(next(iter(s)), i) for i, s in aim.items()], key=lambda p: p[1])
    print(','.join(i for i, _ in ing_all_list))


if __name__ == '__main__':
    solve(common.load_input(2020, 21))
