import python.common as common
import re

data = common.load_input(2020, 5)

passes = data.splitlines()


def seat_id(s):
    row = int(s[:7].replace('F', '0').replace('B', '1'), 2)
    col = int(s[-3:].replace('L', '0').replace('R', '1'), 2)
    return row * 8 + col


def find_gap(nums):
    nums = sorted(nums)
    lo, hi = min(nums), max(nums)
    return list(set(range(lo, hi+1)).difference(set(nums)))[0]


print(max([seat_id(p) for p in passes]))
print(find_gap([seat_id(p) for p in passes]))
