import python.common as common
import re

data = common.load_input(2020, 7)

test_data = """shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags."""


def parse_line(line):
    bag1, rest = line.split(" bags contain ", 1)
    if rest == 'no other bags.':
        return bag1, []
    else:
        bags = [re.match(r"(\d+) (.+) bags?", s).groups() for s in rest.split(', ')]
        return bag1, [{'count': int(groups[0]), 'color': groups[1]} for groups in bags]


def inv_graph(g):
    d = {}
    for node, children in g.items():
        for child in children:
            col = child['color']
            if col in d:
                d[col].append(node)
            else:
                d[col] = [node]
    return d


def walk_graph(g, start):
    stack = [start]
    visited = set()
    while len(stack):
        color = stack.pop()
        children = g[color] if color in g else []
        for col in children:
            visited.add(col)
            stack.append(col)
    return visited


def count_bags(g, color, visited):
    total = 0
    for child in g.get(color, []):
        col = child['color']
        if col in visited:
            total += visited[col] * child['count']
        else:
            v = count_bags(g, col, visited)
            total += v * child['count']
    print(f"{color} contains {total} bags")
    visited[color] = total + 1
    return total + 1


graph = dict(parse_line(l) for l in data.splitlines())

print(len(walk_graph(inv_graph(graph), 'shiny gold')))
count_bags(graph, 'shiny gold', {})
