import python.common as common


def can_sum_to_target(nums, target):
    n = len(nums)
    for i in range(n):
        for j in range(i + 1, n):
            if nums[i] + nums[j] == target:
                return True
    return False


def part1(data, preamble_length=25):
    nums = [int(l) for l in data.splitlines()]
    size = len(nums)
    i = preamble_length
    while i < size:
        target = nums[i]
        if not can_sum_to_target(nums[i - preamble_length:i], target):
            return target
        i += 1


def part2(data, target):
    nums = [int(l) for l in data.splitlines()]
    lo, hi = 0, 1
    while True:
        acc = sum(nums[lo:hi + 1])
        if acc > target:
            lo += 1
        elif acc < target:
            hi += 1
        else:
            break
    return min(*nums[lo:hi + 1])+max(*nums[lo:hi + 1])


answer_1 = part1(common.load_input(2020, 9))
print(answer_1)
print(part2(common.load_input(2020, 9), answer_1))
