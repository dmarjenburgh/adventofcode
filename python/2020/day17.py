import python.common as common
from collections import Counter


def parse_input(data, part):
    r = set()
    for y, row in enumerate(data.splitlines()):
        for x, col in enumerate(row):
            if col == '#':
                r.add((x, y, 0) if part == 1 else (x, y, 0, 0))
    return r


neighbour_deltas = [(dx, dy, dz) for dx in [-1, 0, 1] for dy in [-1, 0, 1] for dz in [-1, 0, 1] if dx or dy or dz]
neighbour_deltas2 = [(dx, dy, dz, dw) for dx in [-1, 0, 1] for dy in [-1, 0, 1] for dz in [-1, 0, 1] for dw in [-1, 0, 1] if dx or dy or dz or dw]


def neighbours(pos):
    x, y, z = pos
    return ((x + dx, y + dy, z + dz) for dx, dy, dz in neighbour_deltas)


def neighbours2(pos):
    x, y, z, w = pos
    return ((x + dx, y + dy, z + dz, w + dw) for dx, dy, dz, dw in neighbour_deltas2)


def step(state, neigbours_fn):
    next_state = set()
    for pos, n in Counter(loc for pos in state for loc in neigbours_fn(pos)).items():
        if pos in state and n in [2, 3]:
            next_state.add(pos)
        elif pos not in state and n == 3:
            next_state.add(pos)
    return next_state


def solve(data, part):
    state = parse_input(data, part)
    neighbours_fn = neighbours if part == 1 else neighbours2
    for _ in range(6):
        state = step(state, neighbours_fn)
    print(len(state))


if __name__ == '__main__':
    solve(common.load_input(2020, 17), 1)
    solve(common.load_input(2020, 17), 2)
