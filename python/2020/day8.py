import python.common as common
import re

data = common.load_input(2020, 8)


def parse_line(line):
    m = re.match(r"(\w+) ([+-]\d+)", line).groups()
    return m[0], int(m[1])


def read_program(data):
    return [parse_line(l) for l in data.splitlines()]


def run_1(program):
    pc, acc = 0, 0
    seen = set()
    while pc not in seen:
        seen.add(pc)
        op, arg = program[pc] if pc < len(program) else ('halt', 0)
        if op == 'acc':
            acc += arg
            pc += 1
        elif op == 'jmp':
            pc += arg
        elif op == 'nop':
            pc += 1
        elif op == 'halt':
            print(f'Halting program: {acc}')
            return pc, acc
        else:
            raise Exception(f"Unknown opcode: {op}")
    return pc, acc


print(run_1(read_program(data)))


def run_2(program):
    for i in range(len(program)):
        altered_program = [l for l in program]
        op, arg = altered_program[i]
        if op == 'jmp':
            altered_program[i] = 'nop', arg
        elif op == 'nop':
            altered_program[i] = 'jmp', arg
        run_1(altered_program)


run_2(read_program(data))