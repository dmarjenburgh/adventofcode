import re
import numpy as np
import python.common as common


def parse_instructions(data):
    return [(m[0], int(m[1])) for m in [re.match(r"([NWESLRF])(\d+)", l).groups() for l in data.splitlines()]]


r90 = np.array([[0, 1], [-1, 0]])

dirs = {
    'N': np.array([0, 1]),
    'S': np.array([0, -1]),
    'E': np.array([1, 0]),
    'W': np.array([-1, 0])
}


def turn_right(state, k):
    state[k] = r90.dot(state[k])


def step(state, instr):
    d, v = instr
    if d in ['N', 'S', 'W', 'E']:
        state['pos'] += dirs[d] * v
    elif d == 'F':
        state['pos'] += state['dir'] * v
    elif d == 'R':
        n = int(v / 90)
        for _ in range(n):
            turn_right(state, 'dir')
    elif d == 'L':
        n = int((3 * v % 360) / 90)
        for _ in range(n):
            turn_right(state, 'dir')


def step2(state, instr):
    d, v = instr
    if d in ['N', 'S', 'W', 'E']:
        state['wp'] += dirs[d] * v
    elif d == 'F':
        state['pos'] += v * state['wp']
    elif d == 'R':
        n = int(v / 90)
        for _ in range(n):
            turn_right(state, 'wp')
    elif d == 'L':
        n = int((3 * v % 360) / 90)
        for _ in range(n):
            turn_right(state, 'wp')


def solve(data, part):
    state = {'pos': np.array([0, 0]), 'dir': np.array([1, 0]), 'wp': np.array([10, 1])}
    ops = parse_instructions(data)
    step_fn = step if part == 1 else step2
    for op in ops:
        step_fn(state, op)
    return int(np.linalg.norm(state['pos'], 1))


if __name__ == '__main__':
    print(solve(common.load_input(2020, 12), 1))
    print(solve(common.load_input(2020, 12), 2))
