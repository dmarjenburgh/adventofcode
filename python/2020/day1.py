import python.common as common

data = common.load_input(2020, 1)
nums = [int(x) for x in data.splitlines()]

print([nums[i] * nums[j] for i in range(len(nums)) for j in range(len(nums)) if i < j and nums[i] + nums[j] == 2020])

print([nums[i] * nums[j] * nums[z] for i in range(len(nums)) for j in range(len(nums)) for z in range(len(nums)) if i < j < z and nums[i] + nums[j] + nums[z] == 2020])
