import python.common as common
import re

data = common.load_input(2020, 6)


def group_answers(raw_data):
    return [s for s in [group.splitlines() for group in raw_data.split('\n\n')]]


def intersect(strings):
    return set.intersection(*[set(s) for s in strings])


print(sum(len(set().union(*g)) for g in group_answers(data)))
print(sum(len(intersect(ss)) for ss in group_answers(data)))
