import python.common as common
import re

data = common.load_input(2020, 2)
policies = data.splitlines()


def is_valid_password(line):
    low, high, letter, password = re.match(r"(\d+)-(\d+) (\w): (\w+)", line).groups()
    return int(low) <= password.count(letter) <= int(high)


def is_valid_password_2(line):
    low, high, letter, password = re.match(r"(\d+)-(\d+) (\w): (\w+)", line).groups()
    i1, i2 = password[int(low) - 1], password[int(high) - 1]
    return (i1 == letter and i2 != letter) or (i1 != letter and i2 == letter)


print(sum(is_valid_password(policy) for policy in policies))
print(sum(is_valid_password_2(policy) for policy in policies))
