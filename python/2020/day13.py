import python.common as common


def parse_data(data):
    n, ids = data.splitlines()
    return int(n), [int(id) if id != 'x' else id for id in ids.split(',')]


def solve(data):
    # part 1
    n, ids = parse_data(data)
    ids1 = [x for x in ids if x != 'x']
    wait_times = [(id - (n % id)) for id in ids1]
    min_wait = min(*wait_times)
    min_id = ids1[wait_times.index(min_wait)]
    print(f"Part 1: {min_id * min_wait}")

    # part 2
    rests = [-ids.index(x) for x in ids1]
    print(f"Part 2: {common.chinese_remainder(rests, ids1)}")


if __name__ == '__main__':
    solve(common.load_input(2020, 13))
