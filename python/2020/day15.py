import python.common as common


def solve(data, part):
    nums = [int(x) for x in data.split(',')]
    state = {}
    for i, x in enumerate(nums):
        state[x] = [i + 1]
    last = nums[-1]
    turn = len(nums)
    # A bit slow, but should be < 1min
    num_turns = 2020 if part == 1 else 30000000
    while turn < num_turns:
        turn += 1
        turns_spoken = state[last]
        if len(turns_spoken) == 1:
            last = 0
        else:
            last = turns_spoken[-1] - turns_spoken[-2]
        if last in state:
            state[last].append(turn)
        else:
            state[last] = [turn]
    print(last)


if __name__ == '__main__':
    solve(common.load_input(2020, 15), part=1)
    solve(common.load_input(2020, 15), part=2)
