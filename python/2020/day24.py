import python.common as common
from functools import reduce
from collections import Counter

# Use basis vectors: [1,0] = e, [0,1] = ne
adjacent_cells = {'e': [1, 0], 'se': [1, -1], 'sw': [0, -1], 'w': [-1, 0], 'nw': [-1, 1], 'ne': [0, 1]}


def parse_line(line):
    r = []
    while line:
        if line[0] in 'ns':
            r.append(adjacent_cells[line[:2]])
            line = line[2:]
        else:
            r.append(adjacent_cells[line[0]])
            line = line[1:]
    return r


def parse_input(data):
    return [parse_line(l) for l in data.splitlines()]


def compose_steps(steps):
    return reduce(lambda acc, b: (acc[0] + b[0], acc[1] + b[1]), steps, (0, 0))


def neighbours(pos):
    x, y = pos
    return ((x + d[0], y + d[1]) for d in adjacent_cells.values())


def step(state):
    nstate = set()
    for pos, n in Counter(n for pos in state for n in neighbours(pos)).items():
        if pos in state and n in [1, 2]:
            nstate.add(pos)
        elif pos not in state and n == 2:
            nstate.add(pos)
    return nstate


def solve(data):
    lines = parse_input(data)
    black_tiles = set()
    for l in lines:
        pos = compose_steps(l)
        if pos in black_tiles:
            black_tiles.remove(pos)
        else:
            black_tiles.add(pos)
    print(len(black_tiles))

    state = black_tiles
    for i in range(100):
        state = step(state)
    print(len(state))


if __name__ == '__main__':
    solve(common.load_input(2020, 24))
