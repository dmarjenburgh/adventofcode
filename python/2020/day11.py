import python.common as common


def parse_state(data):
    grid = {}
    for i, row in enumerate(data.splitlines()):
        for j, col in enumerate(row):
            if col in ['L', '#']:
                grid[(i, j)] = col

    return grid


def neighbours(pos):
    x, y = pos
    return [(x + dx, y + dy) for dx in [-1, 0, 1] for dy in [-1, 0, 1] if dx or dy]


def count_occupied_neighbours(grid, pos):
    x = len([1 for n in neighbours(pos) if n in grid and grid[n] == '#'])
    return x


def nearest_in_dir(grid, pos, dir_):
    x, y = pos
    dx, dy = dir_
    x += dx
    y += dy

    while 0 <= x < 98 and 0 <= y < 98:
        if (x, y) in grid:
            return grid[(x, y)] == '#'
        x += dx
        y += dy
    return False


def count_occupied_neighbours2(grid, pos):
    return sum(nearest_in_dir(grid, pos, d) for d in [[x, y] for x in [-1, 0, 1] for y in [-1, 0, 1] if x or y])


def step(grid, neighbour_count_fn, b):
    ng = {}
    for k, v in grid.items():
        on = neighbour_count_fn(grid, k)
        if v == 'L' and on == 0:
            ng[k] = '#'
        elif v == '#' and on >= b:
            ng[k] = 'L'
        else:
            ng[k] = v
    return ng


def print_grid(g):
    for j in range(10):
        print()
        for i in range(10):
            print(g.get((j, i), '.'), end='')
    print()


def solve(data, part):
    grid = parse_state(data)
    i = 0
    count_fn = count_occupied_neighbours if part == 1 else count_occupied_neighbours2
    b = 4 if part == 1 else 5
    while True:
        i += 1
        ng = step(grid, count_fn, b)
        if ng == grid:
            break
        grid = ng

    print(len([x for x in grid if grid[x] == '#']))


if __name__ == '__main__':
    solve(common.load_input(2020, 11), 1)
    solve(common.load_input(2020, 11), 2)
