import python.common as common

data = common.load_input(2020, 3)
lines = data.splitlines()
mx, my = len(lines[0]), len(lines)

trees = set((x, y) for y in range(my) for x in range(mx) if lines[y][x] == '#')


def num_trees(dx, dy):
    counter = 0
    x, y = 0, 0
    while y < my:
        if (x, y) in trees:
            counter += 1
        x = (x + dx) % mx
        y += dy
    return counter


print(num_trees(3, 1))

print(num_trees(1, 1) * num_trees(3, 1) * num_trees(5, 1) * num_trees(7, 1) * num_trees(1, 2))
