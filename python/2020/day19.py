import python.common as common
import itertools


def parse_input(data):
    p1, p2 = data.split('\n\n')
    rules = {}
    for line in p1.splitlines():
        rule_num, rest = line.split(': ')
        rule_num = int(rule_num)
        if rest.startswith("\""):
            rules[rule_num] = rest[1]
        else:
            parts = rest.split(' | ')
            rules[rule_num] = [[int(x) for x in p.split(' ')] for p in parts]
    messages = p2.splitlines()
    return rules, messages


def topsort(rules):
    visited = set()
    result = []
    stack = [0]
    while len(stack):
        node = stack[-1]
        visited.add(node)
        rule = rules[node]
        if type(rule) == str:
            result.append(stack.pop())
            continue

        neighbours = [n for part in rule for n in part if n not in visited]
        if len(neighbours):
            stack.append(neighbours[0])
        else:
            result.append(stack.pop())
    return result


def possible_messages(rules):
    visit_order = topsort(rules)
    possibilities = {}
    for rnum in visit_order:
        rule = rules[rnum]
        if type(rule) == str:
            possibilities[rnum] = {rule}
        else:
            np = set(x for part in rule for x in itertools.starmap(lambda *a: ''.join(a), itertools.product(*(possibilities[i] for i in part))))
            possibilities[rnum] = np
    return possibilities


def matches_rule(pm, msg, rnum):
    if rnum == 0:
        for i in range(len(msg) - 1):
            s1, s2 = msg[:i + 1], msg[i + 1:]
            if matches_rule(pm, s1, 8) and matches_rule(pm, s2, 11):
                return True
        return False
    elif rnum == 8:
        if msg in pm[8]:
            return True
        else:
            for m in pm[42]:
                if msg.startswith(m):
                    return matches_rule(pm, msg[len(m):], rnum)
            return False
    elif rnum == 11:
        if msg in pm[11]:
            return True
        else:
            for pre, suf in itertools.product(pm[42], pm[31]):
                if msg.startswith(pre) and msg.endswith(suf):
                    return matches_rule(pm, msg[len(pre):-len(suf)], rnum)
    else:
        return msg in pm


def solve(data):
    rules, messages = parse_input(data)
    pm = possible_messages(rules)

    print(f"Part 1: {len([m for m in messages if m in pm[0]])}")
    print(f"Part 2: {len([m for m in messages if matches_rule(pm, m, 0)])}")


if __name__ == '__main__':
    solve(common.load_input(2020, 19))
