import os
import numpy as np


def load_input(year, day):
    path = os.path.join(os.path.dirname(__file__), '../resources')
    with open(f"{path}/{year}/input{day}.txt") as f:
        data = f.read()

    return data


def extended_gcd(a, b):
    """Returns (x,y) such that ax+by=gcd"""
    x = lasty = 0
    y = lastx = 1
    while b != 0:
        q = a // b
        a, b = b, a % b
        x, lastx = lastx - q * x, x
        y, lasty = lasty - q * y, y
    return lastx, lasty


def chinese_remainder(rests, modulos):
    """ Solve a system of linear congruences"""
    assert len(rests) == len(modulos)
    x = 0
    prod = np.prod(modulos)

    for mi, resti in zip(modulos, rests):
        p = prod // mi
        s = extended_gcd(p, mi)[0]
        e = s * p
        x += resti * e
    return x % prod
