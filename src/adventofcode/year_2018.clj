(ns adventofcode.year-2018
  (:require [adventofcode.util :as util]
            [clojure.string :as s]
            [clojure.set :as set]
            [clojure.math.combinatorics :as combo]
            [clojure.walk :as walk])
  (:import [java.util HashMap ArrayList]
           [java.util.concurrent.atomic AtomicInteger AtomicLong]))

(def read-input (partial util/read-input 2018))

(def read-lines (comp s/split-lines read-input))

(defn parse-lines [day f] (mapv f (read-lines day)))
(defn parse-nums [line] (mapv #(Long/parseLong (s/trim %)) (s/split line #",")))

(defn inverse-lookup [m]
  (persistent! (reduce-kv (fn [acc k v] (assoc! acc v ((fnil conj #{}) (get acc v) k))) (transient {}) m)))

(defn day1-1 [input]
  (let [lines (s/split-lines input)
        nums (mapv #(Long/parseLong %) lines)]
    (reduce + nums)))
(defn day1-2 [input]
  (let [lines (s/split-lines input)
        nums (cycle (mapv #(Long/parseLong %) lines))]
    (reduce (fn [[^long acc seen] ^long n]
              (let [acc2 (+ acc n)]
                (if (contains? seen acc2)
                  (reduced acc2)
                  [acc2 (conj seen acc2)])))
            [0 #{0}]
            nums)))

(defn day2-1 [input]
  (letfn [(contains-2 [id] (contains? (into #{} (vals (frequencies id))) 2))
          (contains-3 [id] (contains? (into #{} (vals (frequencies id))) 3))]
    (* (count (filter contains-2 input)) (count (filter contains-3 input)))))

(defn day2-2 [lines]
  (letfn [(differing-chars [s1 s2] (into #{} (keep-indexed (fn [i [c1 c2]] (when (not= c1 c2) i))) (mapv vector s1 s2)))
          (pop-char [s ^long i] (str (subs s 0 i) (subs s (inc i))))]
    (loop [[[id1 id2] & more] (combo/combinations lines 2)]
      (if id1
        (let [dc (differing-chars id1 id2)]
          (if (= (count dc) 1)
            (pop-char id1 (first dc))
            (recur more)))
        :no-result))))

(letfn [(parse-line [line]
          (let [[_ id x y w h] (re-find #"^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$" line)]
            {:id (Long/parseLong id) :x (Long/parseLong x) :y (Long/parseLong y)
             :w (Long/parseLong w) :h (Long/parseLong h)}))
        (coords [claim] (for [yc (range (:h claim))
                              xc (range (:w claim))]
                          [(+ ^long (:x claim) ^long xc) (+ ^long (:y claim) ^long yc)]))]
  (defn day3-1 [lines]
    (let [claims (mapv parse-line lines)
          ]
      (->> claims
           (mapcat coords)
           (frequencies)
           (vals)
           (filter #(> (long %) 1))
           (count))))
  (defn day3-2 [lines]
    (let [claims (mapv parse-line lines)
          coord-freqs (frequencies (mapcat coords claims))
          free? (fn [claim]
                  (every? (fn [[x y]] (= (coord-freqs [x y]) 1)) (coords claim)))]
      (some #(when (free? %) (:id %)) claims))))

(defn- parse-day4-line [line]
  (let [guard-id (fn [s] (Long/parseLong (re-find #"\d+" s)))
        [_ date h m remaining] (re-find #"^\[(\d+-\d+-\d+) (\d+):(\d+)\] (.+)$" line)]
    (merge {:date date :hour (Long/parseLong h) :minute (Long/parseLong m)}
           (cond
             (s/starts-with? remaining "wakes") {:type :wakes}
             (s/starts-with? remaining "falls") {:type :falls-asleep}
             (s/starts-with? remaining "Guard") {:type :begins-shift :id (guard-id remaining)}))))

(defn day4 [input]
  (letfn [(sleeping-minutes [acc e] (let [i (long (:sleep-start acc))
                                          d (- (long (:minute e)) i)]
                                      (vec (range i (+ i d)))))
          (handle-shift [acc e] (assoc acc :current (:id e)))
          (handle-sleep [acc e] (assoc acc :sleep-start (:minute e)))
          (handle-wakes [acc e] (update-in acc [:guards (:current acc) :minutes] (fnil into []) (sleeping-minutes acc e)))]
    (let [events (mapv parse-day4-line (sort (s/split-lines input)))
          summary (reduce (fn [acc e]
                            (case (:type e)
                              :begins-shift (handle-shift acc e)
                              :wakes (handle-wakes acc e)
                              :falls-asleep (handle-sleep acc e))) {} events)
          most-sleepy-minute (fn [id]
                               (let [fs (frequencies (get-in summary [:guards id :minutes]))]
                                 (apply max-key second fs)))]
      {:part-1 (let [most-sleepy-guard (long (first (apply max-key (fn [[k v]] (count (:minutes v))) (seq (:guards summary)))))
                     m (long (first (most-sleepy-minute most-sleepy-guard)))]
                 [most-sleepy-guard m (* most-sleepy-guard m)])
       :part-2 (let [[g m] (apply max-key peek (map (fn [[id m]] (into [id] (most-sleepy-minute id))) (:guards summary)))]
                 [g m (* (long g) (long m))])})))

(defn day5 [polymer]
  (let [letters "abcdefghijklmnopqrstuvwxyz"
        combs (map str letters (s/upper-case letters))
        r (re-pattern (s/join "|" (concat
                                    combs
                                    (map str (s/upper-case letters) letters))))
        results (for [c (into [""] combs)
                      :let [p (s/replace (s/trim polymer) (re-pattern (if (seq c) (str "[" c "]") "")) "")]]
                  (loop [p p]
                    (let [p2 (s/replace p r "")]
                      (if (= p p2)
                        [c (count p)]
                        (recur p2)))))]
    {:part-1 (second (first results))
     :part-2 (apply min-key second results)}))

(defn day6 [part input]
  (let [points (mapv parse-nums (s/split-lines input))
        [^long minx ^long maxx ^long miny ^long maxy] (for [g [first second]
                                                            f [min max]]
                                                        (apply f (map g points)))
        distance (fn [[^long x1 ^long y1] [^long x2 ^long y2]] (+ (Math/abs (- x1 x2)) (Math/abs (- y1 y2))))]
    (if (= part 1)
      (let [closest-coord (fn [p]
                            (let [dists (zipmap points (map (partial distance p) points))
                                  [closest cd] (apply min-key second dists)
                                  first-closest (some #(when (#{cd} (val %)) (key %)) dists)]
                              (when (= closest first-closest) closest)))
            closest-coords (into {} (for [y (range miny (inc maxy))
                                          x (range minx (inc maxx))]
                                      {[x y] (closest-coord [x y])}))
            points-with-area-at-edge (into #{} (for [y (range miny (inc maxy))
                                                     x (range minx (inc maxx))
                                                     :when (or (= y miny)
                                                               (= y maxy)
                                                               (= x minx)
                                                               (= x maxx))
                                                     :let [p (get closest-coords [x y])]
                                                     :when p]
                                                 p))
            coords-with-finite-area-points (apply dissoc (inverse-lookup closest-coords) nil points-with-area-at-edge)]
        (let [[p ps] (apply max-key (comp count second) coords-with-finite-area-points)]
          [p (count ps)]))
      (let [distance-sum (fn [p] (transduce (map (partial distance p)) + 0 points))]
        (->> (map distance-sum (for [y (range miny (inc maxy)) x (range minx (inc maxx))] [x y]))
             (filter #(< (long %) 10000))
             count)))))

(defn- parse-day7-line [line]
  (let [[_ a b] (re-find #"^Step (\w) must be finished before step (\w) can begin." line)]
    [a b]))

(defn day7 [day input]
  (let [edges (mapv parse-day7-line (s/split-lines input))
        terminal-node (let [out (into #{} (map first) edges)
                            in (into #{} (map second) edges)]
                        (first (set/difference in out)))
        nodes (into #{} (flatten edges))
        starting-nodes (fn [es]
                         (let [out (into #{} (map first) es)
                               in (into #{} (map second) es)
                               x (if (seq out)
                                   (set/difference out in)
                                   in)]
                           x))
        edges (conj edges [terminal-node nil])]
    (if (= day 1)
      (loop [edges edges order []]
        (if (seq edges)
          (if-let [sn (first (sort (starting-nodes edges)))]
            (recur (remove (comp #{sn} first) edges) (conj order sn))
            :no-sn)
          (apply str (conj order (first (set/difference nodes (set order)))))))
      (let [num-workers 5
            wstate (fn [n] {:id n :task nil :seconds-left nil})
            find-free-worker (fn [ws] (some #(when (-> % val :task nil?) (val %)) ws))
            assign (fn [w task] (assoc w :task task :seconds-left (- (int (first task)) 5)))
            remove-task (fn [edges task] (remove (comp #{task} first) edges))
            process-tasks (fn [ws]
                            (reduce-kv (fn [[tasks-done workers] k v]
                                         (if (:task v)
                                           (cond
                                             (zero? (long (:seconds-left v))) [(conj tasks-done (:task v)) (assoc workers k (wstate k))]
                                             :else [tasks-done (update-in workers [k :seconds-left] dec)])
                                           [tasks-done workers])) [[] ws] ws))
            active-tasks (fn [ws] (set (keep :task (vals ws))))
            assign-workers (fn [ws tasks]
                             (reduce (fn [ws task]
                                       (if-let [fw (find-free-worker ws)]
                                         (assoc ws (:id fw) (assign fw task))
                                         (reduced ws))) ws tasks))]
        (loop [edges edges
               workers (zipmap (range num-workers) (map wstate (range 5)))
               done ""
               t -1]
          (let [[tasks-done workers] (process-tasks workers)
                edges (reduce remove-task edges tasks-done)
                done (apply str done tasks-done)]
            (if (= (count done) (count nodes))
              [done (inc t)]
              (let [tasks (sort (remove (active-tasks workers) (starting-nodes edges)))
                    workers (assign-workers workers tasks)]
                (recur edges workers done (inc t))))))))))

(defn day8 [input]
  (let [license (mapv #(Long/parseLong %) (re-seq #"\d+" input))
        build-tree (fn build-tree [data]
                     (let [[qc qm & rest] data]
                       (loop [rest rest chs [] ^long i qc]
                         (if (pos? i)
                           (let [[fchild & r] (build-tree rest)]
                             (recur r (conj chs fchild) (dec i)))
                           (into [{:header [qc qm] :children chs :meta (vec (take qm rest))}] (drop qm rest))))))
        tree (first (build-tree license))
        part-1 (fn [node] (walk/postwalk (fn [x]
                                           (if (map? x)
                                             (reduce + 0 (concat (:children x) (:meta x)))
                                             x)) node))
        part-2 (fn node-value ^long [node]
                 (if (empty? (:children node))
                   (reduce + 0 (:meta node))
                   (let [meta (:meta node)]
                     (reduce (fn [^long acc ^long m]
                               (if-let [x (get (:children node) (dec m))]
                                 (+ acc ^long (node-value x))
                                 acc)) 0 meta))))]
    [(part-1 tree) (part-2 tree)]))

(defn parse-day9 [text]
  (zipmap [:num-players :last-marble] (map #(Long/parseLong %) (re-seq #"\d+" text))))

(defn day9 [part input]
  (let [{:keys [^long num-players last-marble]} (parse-day9 input)
        last-marble (long (if (= part 1) last-marble (* last-marble 100)))
        links (long-array (repeat (inc last-marble) -1)) ;; e.g. if marble 5 points to marble 25, then links[5] == 25
        rlinks (long-array (repeat (inc last-marble) -1)) ;; rlinks[25] == 5
        _ (aset links 0 0)
        _ (aset rlinks 0 0)
        initial-state {:scores {} :player -1, :current 0}
        insert! (fn [cur ^long x]
                  (let [next-marble (aget links cur)
                        nnext-marble (aget links next-marble)]
                    (aset links next-marble x)
                    (aset rlinks x next-marble)
                    (aset links x nnext-marble)
                    (aset rlinks nnext-marble x)))
        show (fn [state]
               (if (= 0 (:current state))
                 (printf "(%2d)" 0)
                 (printf " %2d " 0))
               (loop [x (aget links 0)]
                 (when (pos? x)
                   (if (= x (:current state))
                     (printf "(%2d)" x)
                     (printf " %2d " x))
                   (recur (aget links x))))
               (println))
        delete! (fn [cur]
                  (let [r ^long (nth (iterate #(aget rlinks %) cur) 6)
                        v (aget rlinks r)
                        rr (aget rlinks v)]
                    (aset links rr r)
                    (aset rlinks r rr)
                    v))
        step (fn [state ^long v]
               (let [cur (long (:current state))
                     player (rem (inc ^long (:player state)) num-players)]
                 (if (zero? (rem v 23))
                   (let [score (delete! cur)]
                     (assoc state
                       :player player
                       :scores (update (:scores state) player (fnil + 0) score v)
                       :current (aget links score)))
                   (let [_ (do (insert! cur v))]
                     (assoc state :player player :current v)))))]
    (loop [state initial-state value 1]
      ;(show state)
      (if (<= value last-marble)
        (recur (step state value) (inc value))
        (apply max (vals (:scores state)))))))

(defn parse-day10-line [line]
  (let [[_ x y vx vy] (re-find #"position=<\s*(-?\d+),\s*(-?\d+)> velocity=<\s*(-?\d+),\s*(-?\d+)>" line)]
    [[(Long/parseLong (s/trim x)) (Long/parseLong (s/trim y))] [(Long/parseLong (s/trim vx)) (Long/parseLong (s/trim vy))]]))

(defn day10 [stars t]
  (let [initial-positions (mapv first stars)
        velocities (mapv second stars)
        bounding-box-dims (fn [positions]
                            (let [minx (apply min (map first positions))
                                  maxx (apply max (map first positions))
                                  miny (apply min (map second positions))
                                  maxy (apply max (map second positions))]
                              {:min-x minx :max-x maxx :min-y miny :max-y maxy}))
        move (fn [positions]
               (mapv (fn [[^long x ^long y] [^long vx ^long vy]] [(+ x vx) (+ y vy)]) positions velocities))
        msg-positions (into #{} (nth (iterate move initial-positions) t))
        msg-bb (bounding-box-dims msg-positions)]
    (doseq [y (range (:min-y msg-bb) (inc ^long (:max-y msg-bb)))]
      (println)
      (doseq [x (range (:min-x msg-bb) (inc ^long (:max-x msg-bb)))]
        (print (if (contains? msg-positions [x y]) "*" " "))))
    (println "\n")))

(comment

  (day1-1 (read-input 1))
  (day1-2 (read-input 1))

  (day2-1 (read-lines 2))
  (day2-2 (read-lines 2))

  (day3-1 (read-lines 3))
  (day3-2 (read-lines 3))

  (day4 (read-input 4))

  (day5 (read-input 5)) ; very slow

  (day6 1 (read-input 6))
  (day6 2 (read-input 6))

  (day7 1 (read-input 7))
  (day7 2 (read-input 7))

  (day8 (read-input 8))

  (day9 1 (read-input 9))
  (day9 2 (read-input 9))

  (day10 (parse-lines 10 parse-day10-line) 10641)

  )

(defn day11 [serial-number & [{:keys [grid-size]}]]
  (let [serial-num (long serial-number)
        power-level (fn [[^long x ^long y]]
                      (let [rack-id (+ x 10)]
                        (- (rem (quot (* rack-id (+ (* rack-id y) serial-num)) 100) 10) 5)))
        coords (for [x (range 1 301) y (range 1 301)] [x y])
        pl-table (zipmap coords (map power-level coords))
        integral-grid-power (reduce (fn [acc [^long x ^long y]]
                                      (assoc acc [x y]
                                        (- (+ ^long (pl-table [x y])
                                              ^long (acc [(dec x) y] 0)
                                              ^long (acc [x (dec y)] 0))
                                           ^long (acc [(dec x) (dec y)] 0)))) {} coords)
        sat (fn [[^long left-x ^long top-y ^long size]]
              (let [d ^long (dec size)]
                (- (+ ^long (integral-grid-power [(+ left-x d) (+ top-y d)] 0)
                      ^long (integral-grid-power [(dec left-x) (dec top-y)] 0))
                   ^long (integral-grid-power [(+ left-x d) (dec top-y)] 0)
                   ^long (integral-grid-power [(dec left-x) (+ top-y d)] 0))))
        grid-power (fn [[^long left-x ^long top-y ^long size]]
                     (if (> size 1)
                       (sat [left-x top-y size])
                       (pl-table [left-x top-y])))
        all-grids (for [^long size (if grid-size [grid-size] (range 1 301))
                        :let [_ (println "Processing grid size" size)]
                        x (range 1 (- 302 size))
                        y (range 1 (- 302 size))]
                    [x y size])]
    (apply max-key grid-power all-grids)))

(defn parse-day12 [input]
  (let [[init _ & rule-lines] (s/split-lines input)
        [_ pots] (re-find #"initial state: (.*)$" init)
        initial-state (into #{} (keep-indexed (fn [i c] (when (= c \#) i))) pots)
        parse (fn [rule] (into #{}
                               (keep-indexed (fn [^long i c] (when (= c \#) (- i 2))))
                               (re-find #"^[.#]{5}" rule)))]
    [initial-state (into #{} (comp
                               (filter #(s/ends-with? % "#"))
                               (map parse)) rule-lines)]))

(defn day12 [input]
  (let [[initial-state spawn-conditions] (parse-day12 input)
        step (fn [plants]
               (let [^long min-pot (apply min Long/MAX_VALUE plants)
                     ^long max-pot (apply max Long/MIN_VALUE plants)]
                 (into #{} (for [^long pot (range (- min-pot 2) (+ max-pot 3))
                                 :let [neighbourhood (into #{} (comp
                                                                 (filter plants)
                                                                 (map #(- ^long % pot)))
                                                           (range (- pot 2) (+ pot 3)))]
                                 :when (contains? spawn-conditions neighbourhood)]
                             pot))))
        states (iterate step initial-state)
        score (fn [^long t]
                (if (< t 196) ; after t = 196, number of plants increases with 53
                  (reduce + (nth states t))
                  (+ ^long (reduce + (nth states 195)) (* 53 (- t 195)))))]
    {:part-1 (score 20)
     :part-2 (score 50000000000)}))

(defn parse-day13 [input]
  (let [grid (to-array-2d (s/split-lines input))
        [nrows ncols] [(alength grid) (alength ^objects (first grid))]
        cars (into (sorted-set-by #(compare (first %1) (first %2)))
                   (doall (for [r (range nrows) c (range ncols)
                                :let [x (aget grid r c)]
                                :when (#{\< \> \^ \v} x)
                                :let [_ (aset grid r c ({\< \- \> \- \^ \| \v \|} x))]]
                            [[r c] x :left])))]
    [grid cars]))

(defn day13 [input]
  (let [[grid initial-state] (parse-day13 input)
        turn (fn [da] (case da
                        [\^ :straight] [\^ :right]
                        [\v :straight] [\v :right]
                        [\< :straight] [\< :right]
                        [\> :straight] [\> :right]
                        [\^ :left] [\< :straight]
                        [\v :left] [\> :straight]
                        [\< :left] [\v :straight]
                        [\> :left] [\^ :straight]
                        [\^ :right] [\> :left]
                        [\v :right] [\< :left]
                        [\< :right] [\^ :left]
                        [\> :right] [\v :left]))
        move (fn [[pos dir action]]
               (let [[i f] ({\^ [0 dec] \v [0 inc] \< [1 dec] \> [1 inc]} dir)
                     pos2 (update pos i f)
                     ground (apply aget grid pos2)
                     [dir2 action2] (case ground
                                      (\| \-) [dir action]
                                      \/ [({\^ \> \< \v \v \< \> \^} dir) action]
                                      \\ [({\^ \< \< \^ \> \v \v \>} dir) action]
                                      \+ (turn [dir action]))]
                 [pos2 dir2 action2]))
        tick (fn [cars]
               (loop [[car & more] cars all cars]
                 (if car
                   (let [mcar (move car)
                         collision-car (some #(when (= (first %) (first mcar)) %) all)]
                     (if collision-car
                       [:boom-between mcar collision-car]
                       (recur more (-> all (disj car) (conj mcar)))))
                   all)))
        tick2 (fn [cars]
                (loop [[car & more] cars all cars crashed #{}]
                  (if car
                    (if (crashed car)
                      (recur more all crashed)
                      (let [mcar (move car)
                            collision-car (some #(when (= (first %) (first mcar)) %) all)]
                        (if collision-car
                          (let [after-boom (-> all (disj car collision-car))]
                            (println (format "Car %s collided with %s. %d cars left." car collision-car (count after-boom)))
                            (recur more after-boom (conj crashed collision-car)))
                          (recur more (-> all (disj car) (conj mcar)) crashed))))
                    all)))]
    {:part-1 (loop [state initial-state t 0]
               (if (= (first state) :boom-between)
                 (first (second state))
                 (recur (tick state) (inc t))))
     :part-2 (loop [state initial-state t 0]
               (if (<= (count state) 1)
                 (ffirst state)
                 (recur (tick2 state) (inc t))))}))

(defn day14 [input]
  (let [recipes-gen (fn []
                      (let [elves (long-array [0 1])
                            rs (doto (ArrayList. (int 1e8)) (.add 3) (.add 7))]
                        (fn []
                          (let [one (aget elves 0)
                                two (aget elves 1)
                                v-one ^long (.get rs one)
                                v-two ^long (.get rs two)
                                sum (+ v-one v-two)
                                digits (mapv #(Character/digit ^char % 10) (str sum))
                                _ (.addAll rs digits)
                                newSize (.size rs)]
                            (aset elves 0 ^long (rem (+ one 1 v-one) newSize))
                            (aset elves 1 ^long (rem (+ two 1 v-two) newSize))
                            digits))))
        recipes-seq (fn []
                      (let [rg (recipes-gen)
                            seq-g (fn rf []
                                    (lazy-seq
                                      (let [digits (rg)]
                                        (concat digits (rf)))))]
                        (cons 3 (cons 7 (seq-g)))))]
    {:part-1 (apply str (take 10 (drop (Long/parseLong input) (recipes-seq))))
     :part-2 (let [pattern (mapv #(Character/digit ^char % 10) input)
                   first-digit (first pattern)
                   pattern-length (count pattern)]
               (loop [rs (recipes-seq) n 0]
                 (if (= (take pattern-length rs) pattern)
                   n
                   (let [skip (max (nth rs first-digit) 1)]
                     (recur (drop skip rs) (+ n skip))))))}))

(defn parse-day15 [input]
  (let [field ^"[[C" (into-array (map #(into-array Character/TYPE %) (s/split-lines input)))
        nrows (alength field)
        ncols (alength ^chars (first field))
        units (HashMap.)
        _ (doseq [r (range nrows) c (range ncols)
                  :let [g (aget field r c)]
                  :when (#{\G \E} g)]
            (aset field r c \.)
            (.put units [r c] (doto (HashMap.) (.put :type g) (.put :hp 200))))]
    [field units]))
(defn day15 [[^"[[C" field ^HashMap units]]
  (letfn [(enemy-type [unit] ({\G \E \E \G} (:type unit)))
          (tile-type [p] (get-in units [p :type] (apply aget field p)))
          (squares-in-range [unit] (let [enemy-type (enemy-type unit)
                                         positions (into [] (comp
                                                              (filter (comp #{enemy-type} :type val))
                                                              (map key)) units)]
                                     (doall (for [p positions
                                                  dir [[-1 0] [0 -1] [0 1] [1 0]]
                                                  :let [sq (mapv + p dir)]
                                                  :when (= \. (tile-type sq))]
                                              sq))))
          (next-step [from]
            (let [unit (.get units from)
                  in-range (squares-in-range {:type (:type unit) :pos from})
                  dirs [[1 0] [0 1] [0 -1] [-1 0]]
                  paths (fn [sq to]
                          (util/a*-search sq to (fn [p]
                                                  (for [dir [[1 0] [0 1] [0 -1] [-1 0]]
                                                        :let [sq (mapv + p dir)]
                                                        :when (= \. (tile-type sq))]
                                                    sq)) (constantly 1) util/manhattan-distance))
                  all-paths (for [dir dirs
                                  :let [sq (mapv + from dir)]
                                  :when (= \. (tile-type sq))
                                  to in-range
                                  :let [path (paths sq to)]
                                  :when (not= path :target-node-not-found)]
                              path)
                  min-length (if (seq all-paths) (apply min (map count all-paths)) -1)
                  shortest-paths (into [] (remove #(> (long (count %)) ^long min-length)) all-paths)]
              (ffirst (sort-by first shortest-paths))))
          (adjacent-enemy [pos]
            (let [unit (.get units pos)
                  enemy-type (enemy-type unit)
                  dirs [[-1 0] [0 -1] [0 1] [1 0]]
                  enemies (vec (reverse (sort-by first (for [dir dirs
                                                             :let [epos (mapv + pos dir)]
                                                             :when (= enemy-type (tile-type epos))]
                                                         [epos (.get units epos)]))))]
              (when (seq enemies) (first (apply min-key (comp :hp second) enemies)))))
          (attack! [pos epos]
            (let [unit (.get units pos)
                  enemy ^HashMap (.get units epos)
                  _ (println "Unit" (:type unit) "at" pos "attacks" (.toString enemy) "at" epos)
                  _ (.put enemy :hp (- ^long (:hp enemy) ^long ({\G 3 \E 19} (:type unit))))]
              (when (<= ^long (:hp enemy) 0)
                (println "The" (:type enemy) "dies")
                (when (#{\E} (:type enemy)) (throw (Exception. "An elf died...")))
                (.remove units epos))))
          (move! [pos]
            (if (adjacent-enemy pos)
              pos
              (let [step (next-step pos)
                    unit (.get units pos)]
                (if step
                  (do
                    (println "Move" (:type unit) "from" pos "to" step)
                    (doto units (.remove pos) (.put step unit))
                    step)
                  pos))))
          (round! []
            (let [starting-positions (sort (keys units))]
              (doseq [pos starting-positions
                      :let [g (tile-type pos)]
                      :when (#{\G \E} g)
                      :let [newpos (move! pos)
                            ae (adjacent-enemy newpos)
                            _ (when (and (= pos newpos) (not ae))
                                (println "Unit" g "at" pos "not doing anything"))]]
                (when ae (attack! newpos ae))))
            field)
          (battle-finished []
            (when (= 1 (count (into #{} (map :type) (vals units))))
              [(-> units first val :type) (transduce (map :hp) + 0 (vals units))]))
          (print-field []
            (doseq [r (range (alength field))
                    :let [_ (println)]
                    c (range (alength ^chars (get field 0)))
                    :let [g (tile-type [r c])]]
              (print g))
            (println)
            (flush))]
    (print-field)
    (loop [i 0]
      (println i "rounds completed")
      (print-field)
      (doseq [k (sort (keys units))]
        (println k (.toString (get units k))))
      (if-let [report (battle-finished)]
        (do
          (println (zipmap (keys units) (map #(into {} %) (vals units))))
          (conj report i))
        (when (< i 1000) (round!) (recur (inc i)))))))

(defn parse-day16 [input]
  (let [parts (partition-by empty? (s/split-lines input))
        [p1 p2] (let [[fp [_ lp]] (split-with (complement #{["" "" ""]}) parts)]
                  [fp lp])
        parse-part-1 (fn [[br rr ar]]
                       (let [b (mapv #(Long/parseLong %) (re-seq #"\d+" br))
                             a (mapv #(Long/parseLong %) (re-seq #"\d+" ar))
                             r (zipmap [:op :a :b :c] (mapv #(Long/parseLong %) (s/split rr #"\s+")))]
                         [b a r]
                         (zipmap [:instruction :before :after] [r b a])))]
    {:samples (into [] (comp (remove #{[""]}) (map parse-part-1)) p1)
     :test-program (mapv parse-nums p2)}))
(defn day16 [{:keys [samples test-program]}]
  (let [ops [:addr :addi :mulr :muli :banr :bani :borr :bori
             :setr :seti :gtir :gtri :gtrr :eqir :eqri :eqrr]
        do-op (fn [r {:keys [op ^long a ^long b ^long c]}]
                (case op
                  :addr (assoc r c (+ ^long (r a) ^long (r b)))
                  :addi (assoc r c (+ ^long (r a) b))
                  :mulr (assoc r c (* ^long (r a) ^long (r b)))
                  :muli (assoc r c (* ^long (r a) b))
                  :banr (assoc r c (bit-and ^long (r a) ^long (r b)))
                  :bani (assoc r c (bit-and ^long (r a) b))
                  :borr (assoc r c (bit-or ^long (r a) ^long (r b)))
                  :bori (assoc r c (bit-or ^long (r a) b))
                  :setr (assoc r c ^long (r a))
                  :seti (assoc r c a)
                  :gtir (assoc r c (if (> a ^long (r b)) 1 0))
                  :gtri (assoc r c (if (> ^long (r a) b) 1 0))
                  :gtrr (assoc r c (if (> ^long (r a) ^long (r b)) 1 0))
                  :eqir (assoc r c (if (= a ^long (r b)) 1 0))
                  :eqri (assoc r c (if (= ^long (r a) b) 1 0))
                  :eqrr (assoc r c (if (= ^long (r a) ^long (r b)) 1 0))))
        behave (fn [sample]
                 (into #{}
                       (filter #(= (do-op (:before sample) (assoc (:instruction sample) :op %))
                                   (:after sample)))
                       ops))
        eliminate (fn [state sample matching]
                    (let [newstate (update state (:op sample) set/intersection matching)
                          possible-ops (newstate (:op sample))]
                      (if (= (count possible-ops) 1)
                        (reduce (fn [acc i]
                                  (update acc i set/difference possible-ops))
                                newstate
                                (remove #{(:op sample)} (range 16)))
                        newstate)))
        learn-ops (fn [state]
                    (loop [[sample & more] samples state state]
                      (if sample
                        (let [result (eliminate state (:instruction sample) (behave sample))]
                          (recur more result))
                        (mapv (fn [[_ v]] (first v)) (sort state)))))
        initial-state (zipmap (range 16) (repeat (set ops)))]
    {:part-1 (count (into [] (comp (map behave) (map count) (filter (partial <= 3))) samples))
     :part-2 (let [ops (learn-ops initial-state)
                   program (into []
                                 (map #(zipmap [:op :a :b :c] (update % 0 ops)))
                                 test-program)]
               (reduce do-op [0 0 0 0] program))}))

(defn parse-day17 [input]
  (letfn [(coords [line]
            (let [[_ pt cpt rpt lb ub] (re-find #"^([xy])=(\d+), ([xy])=(\d+)\.\.(\d+)$" line)
                  k1 (keyword (str pt))
                  k2 (keyword (str rpt))
                  cv (Long/parseLong cpt)]
              (for [c (range (Long/parseLong lb) (inc (Long/parseLong ub)))
                    :let []]
                (mapv {k1 cv k2 c} [:x :y]))))]
    (let [clay (into #{} (mapcat coords) (s/split-lines input))]
      {:clay-coords clay
       :minx (apply min (map first clay))
       :maxx (apply max (map first clay))
       :miny (apply min (map second clay))
       :maxy (apply max (map second clay))})))

(defn day17 [{:keys [clay-coords minx maxx miny maxy]}]
  (println minx maxx)
  (println))

(comment

  (day11 6878 {:grid-size 3})
  (day11 6878)

  (day12 (read-input 12))

  (day13 (read-input 13))

  (day14 "110201")

  (day15 (parse-day15 (read-input 15)))
  (day15 (parse-day15 (slurp "resources/test15-1.txt")))
  (day15 (parse-day15 (slurp "resources/test15-2.txt")))
  (day15 (parse-day15 (slurp "resources/test15-3.txt")))
  (day15 (parse-day15 (slurp "resources/test15-4.txt")))
  (day15 (parse-day15 (slurp "resources/test15-5.txt")))
  (day15 (parse-day15 (slurp "resources/test15-6.txt")))

  (day16 (parse-day16 (read-input 16)))
  (day17 (parse-day17 (read-input 17)))
  )

