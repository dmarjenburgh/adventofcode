(ns adventofcode.year-2019
  (:require [adventofcode.util :as util]
            [clojure.core.async :as async]
            [clojure.math.combinatorics :as comb]
            [clojure.string :as s]
            [clojure.set :as set]
            [mikera.image.core :as img])
  (:import [clojure.lang PersistentQueue]))

(def read-input (partial util/read-input 2019))

(defn day1 [part input]
  (let [masses (util/parse-lines (comp first util/parse-nums) input)
        fuel (fn [m] (- (quot m 3) 2))
        fuel2 (fn [m] (util/sum (take-while pos? (rest (iterate fuel m)))))]
    (case part
      1 (util/sum (map fuel masses))
      2 (util/sum (map fuel2 masses)))))

(defn- day2-run-program [state]
  (let [get-next-state-vals (fn [from n] (mapv #(aget state (+ from %)) (range 1 (inc n))))
        do-op! (fn [pc code]
                 (case code
                   1 (let [[a b c] (get-next-state-vals pc 3)
                           r (+ (aget state a) (aget state b))]
                       (aset state c r) (+ pc 4))
                   2 (let [[a b c] (get-next-state-vals pc 3)
                           r (* (aget state a) (aget state b))]
                       (aset state c r) (+ pc 4))))]
    (loop [pc 0]
      (let [opcode (aget state pc)]
        (if (= opcode 99)
          (aget state 0)
          (recur (do-op! pc opcode)))))))

(defn day2 [part input & [expected-output]]
  (let [program (util/parse-nums input)
        init-state (fn [noun verb]
                     (let [state (int-array program)]
                       (doto state (aset 1 noun) (aset 2 verb))))]
    (case part
      1 (day2-run-program (init-state 12 2))
      2 (let [noun-verb-pairs (for [v (range (count program)) n (range (count program))] [n v])]
          (some (fn [[n v]]
                  (when (= (day2-run-program (init-state n v)) expected-output)
                    (+ (* 100 n) v))) noun-verb-pairs)))))

(defn day3 [part input]
  (let [parse-coord (fn [s]
                      (let [r (Long/parseLong (subs s 1))]
                        (case (first s)
                          \U [[0 1] r] \D [[0 -1] r]
                          \L [[-1 0] r] \R [[1 0] r])))
        [wire1 wire2] (->> input (s/split-lines) (map (comp (partial map parse-coord) #(s/split % #","))))
        path (fn [wire]
               (loop [[[[dx dy] r] & more] wire [x y] [0 0] visited []]
                 (if dx
                   (let [path (mapv (fn [d] [(+ x (* d dx)) (+ y (* d dy))]) (range 1 (inc r)))]
                     (recur more (peek path) (into visited path)))
                   visited)))
        norm (fn [[x y]] (+ (Math/abs (int x)) (Math/abs (int y))))]
    (case part
      1 (apply min (map norm (set/intersection (set (path wire1)) (set (path wire2)))))
      2 (let [[path1 path2] (map path [wire1 wire2])
              intersections (set/intersection (set path1) (set path2))
              signal-delay (fn [x] (+ (inc (.indexOf path1 x)) (inc (.indexOf path2 x))))]
          (apply min (map signal-delay intersections))))))

(defn day4 [part input]
  (let [digits (fn [s] (mapv #(Character/digit ^Character % 10) s))
        [from to] (mapv digits (s/split input #"-"))
        filt (case part 1 #(< (count (set %)) 6) #(contains? (set (vals (frequencies %))) 2))]
    (count (for [d1 (range 0 10)
                 d2 (range d1 10)
                 d3 (range d2 10)
                 d4 (range d3 10)
                 d5 (range d4 10)
                 d6 (range d5 10)
                 :let [d [d1 d2 d3 d4 d5 d6]]
                 :when (<= (compare from d) 0 (compare to d))
                 :when (filt d)]
             d))))

(def ^:dynamic *relative-base* nil)
(defn- run-intcode-machine
  "Runs the intcode machine. Takes inputs from input-chan and writes outputs to output-chan.
   Will return :halt when opcode 99 is encountered and closes the output channel."
  [input-chan output-chan state]
  (letfn [(get-val [state pc mode] (let [v (aget state pc)]
                                     (case mode 0 (aget state v) 1 v 2 (aget state (+ @*relative-base* v)))))
          (param-modes [icode n] (let [pms (mapv #(Character/digit ^Character % 10) (str (quot icode 100)))]
                                   (into (vec (repeat (- n (count pms)) 0)) (take n pms))))]
    (binding [*relative-base* (atom 0)]
      (async/go-loop [pc 0]
        (let [icode (aget state pc)
              opcode (rem icode 100)
              pmodes (param-modes icode ({1 3 2 3 3 1 4 1 5 2 6 2 7 3 8 3 9 1 99 0} opcode))]
          (case opcode
            (1 2) (let [[m3 m2 m1] pmodes
                        a (get-val state (inc pc) m1)
                        b (get-val state (+ pc 2) m2)
                        v ((case opcode 1 + *) a b)
                        addr (cond-> (aget state (+ pc 3))
                               (= m3 2) (+ @*relative-base*))]
                    (aset state addr v)
                    (recur (+ pc 4)))
            3 (if-some [input (async/<! input-chan)]
                (let [[m] pmodes
                      v (cond-> (aget state (inc pc))
                          (= m 2) (+ @*relative-base*))]
                  (aset state v input)
                  (recur (+ pc 2)))
                (do
                  (async/close! output-chan)
                  :input-channel-closed))
            4 (let [output (get-val state (inc pc) (first pmodes))]
                (async/>! output-chan output)
                (recur (+ pc 2)))
            (5 6) (let [[m2 m1] pmodes
                        a (get-val state (inc pc) m1)]
                    (recur (if ((case opcode 5 (complement zero?) zero?) a)
                             (get-val state (+ pc 2) m2)
                             (+ pc 3))))
            (7 8) (let [[m3 m2 m1] pmodes
                        a (get-val state (inc pc) m1)
                        b (get-val state (+ pc 2) m2)
                        v (cond-> (aget state (+ pc 3))
                            (= m3 2) (+ @*relative-base*))]
                    (aset state v (if ((case opcode 7 < =) a b) 1 0))
                    (recur (+ pc 4)))
            9 (let [[m] pmodes
                    a (get-val state (inc pc) m)]
                (swap! *relative-base* + a)
                (recur (+ pc 2)))
            99 (do (async/close! output-chan) :halt)))))))

(defn- day5 [part input]
  (let [program (util/parse-nums input)
        state (int-array program)
        in-chan (async/chan)
        out-chan (async/chan)]
    (async/onto-chan in-chan [(case part 1 1 5)])
    (let [ouput-coll-chan (async/into [] out-chan)
          r (async/<!! (run-intcode-machine in-chan out-chan state))]
      (async/close! in-chan)
      (async/close! out-chan)
      {:result r
       :outputs (async/<!! ouput-coll-chan)})))

(defn- day6 [part input]
  (let [nodes (into {} (map (fn [[p n]] [n p])) (util/parse-lines #(s/split % #"\)") input))
        nodes-to-com (fn [n] (set (take-while some? (rest (iterate nodes n)))))]
    (case part
      1 (transduce (map (comp count nodes-to-com key)) + nodes)
      2 (let [[p1 p2] (map nodes-to-com ["YOU" "SAN"])]
          (count (set/difference (set/union p1 p2) (set/intersection p1 p2)))))))

(defn- day7 [part input]
  (let [program (util/parse-nums input)
        new-state #(int-array program)
        phase-permutations (case part
                             1 (comb/permutations [0 1 2 3 4])
                             2 (comb/permutations [5 6 7 8 9]))
        init-amps (fn [n]
                    (let [amps (reduce
                                 (fn [acc _]
                                   (conj acc {:state (new-state) :in (:out (peek acc)) :out (async/chan)}))
                                 []
                                 (range n))]
                      (assoc-in amps [0 :in] (async/chan))))
        run-amps (fn [amps phases]
                   (doseq [[a p] (zipmap amps phases)] (async/put! (:in a) p))
                   (mapv (fn [a] (run-intcode-machine (:in a) (:out a) (:state a))) amps))
        close-amps (fn [amps] (doseq [a amps] (async/close! (:out a))))]
    (case part
      1 (let [run-perm (fn [perm]
                         (let [amps (init-amps 5)]
                           (run-amps amps perm)
                           (async/onto-chan (get-in amps [0 :in]) [0])
                           (async/go (let [v (async/<! (:out (peek amps)))]
                                       (close-amps amps)
                                       v))))]
          (apply max (mapv #(async/<!! (run-perm %)) phase-permutations)))
      2 (let [run-perm (fn [perm]
                         (let [amps (init-amps 5)
                               results (run-amps amps perm)
                               amp-e (:out (peek amps))]
                           (async/put! (get-in amps [0 :in]) 0)
                           (async/take! (peek results) (fn [_] (async/close! (get-in amps [0 :in]))))
                           (async/go-loop []
                             (let [v (async/<! amp-e)]
                               (if (async/offer! (get-in amps [0 :in]) v)
                                 (recur)
                                 v)))))]
          (apply max (mapv #(async/<!! (run-perm %)) phase-permutations))))))

(defn day8 [part input]
  (let [[w h] [25 6]
        layers (into [] (partition-all (* w h)) (s/trim input))
        layer-dist (fn [l] (frequencies l))]
    (case part
      1 (let [fewest-0-layer-dist (apply min-key #(get % \0) (mapv layer-dist layers))]
          (* (fewest-0-layer-dist \1) (fewest-0-layer-dist \2)))
      2 (let [visible-layer (reduce (fn [acc l]
                                      (mapv (fn [a b] (if (= a \2) b a)) acc l))
                                    (repeat (* w h) \2) layers)
              display (fn [l] (doseq [l (into []
                                              (comp (map {\0 \space \1 \u25A0})
                                                    (partition-all w)
                                                    (map s/join)) l)]
                                (println l)))]
          (display visible-layer)))))

(defn day9 [part input]
  (let [program (util/parse-nums input)
        new-state #(to-array (into program (repeat 1e6 0)))
        in-chan (async/chan)
        out-chan (async/chan)]
    (async/onto-chan in-chan [part])
    (async/take! (async/into [] out-chan) prn)
    (let [res (async/<!! (run-intcode-machine in-chan out-chan (new-state)))]
      (async/close! in-chan)
      res)))

(defn day10 [part input]
  (let [lines (util/parse-lines input)
        asteroids (into #{} (comp
                              (map-indexed
                                (fn [y l]
                                  (keep-indexed (fn [x c] (when (#{\#} c) [x y])) l)))
                              cat) lines)
        dir (fn [[x1 y1] [x2 y2]] (let [dy (- y2 y1)
                                        dx (- x2 x1)
                                        [d] (util/extended-gcd dy dx)]
                                    (if (zero? d) [0 0] [(/ dx d) (/ dy d)])))
        theta (fn [[x1 y1] [x2 y2]] (let [dx (- x2 x1)]
                                      (if (neg? dx)
                                        (+ (* 2 Math/PI) (Math/atan2 dx (- y1 y2)))
                                        (Math/atan2 dx (- y1 y2)))))
        dist (fn [[x1 y1] [x2 y2]] (Math/hypot (- x2 x1) (- y1 y2)))
        num-ast (fn [pos] (dec (count (into #{} (map (partial dir pos)) asteroids))))]
    (case part
      1 (apply max-key peek (for [pos asteroids]
                              [pos (num-ast pos)]))
      2 (let [loc (first (day10 1 input))
              order (vec (sort (group-by (partial theta loc) (sort-by (partial dist loc) asteroids))))]
          (loop [order order ast []]
            (if (seq order)
              (recur (into [] (keep (fn [[t locs]]
                                      (when (seq (rest locs))
                                        [t (vec (rest locs))]))) order)
                     (into ast (mapv (comp first second) order)))
              (rest ast)))))))
(comment

  (day1 1 (read-input 1))
  (day1 2 (read-input 1))

  (day2 1 (read-input 2))
  (day2 2 (read-input 2) 19690720)

  (day3 1 (read-input 3))
  (day3 2 (read-input 3))

  (day4 1 (read-input 4))
  (day4 2 (read-input 4))

  (day5 1 (read-input 5))
  (day5 2 (read-input 5))

  (day6 1 (read-input 6))
  (day6 2 (read-input 6))

  (day7 1 (read-input 7))
  (day7 2 (read-input 7))

  (day8 1 (read-input 8))
  (day8 2 (read-input 8))

  (day9 1 (read-input 9))
  (day9 2 (read-input 9))

  (day10 1 (read-input 10))
  (day10 2 (read-input 10))
  )

(defn day11 [part input]
  (let [program (util/parse-nums input)
        state (long-array (into program (repeat 1000 0)))
        turn (fn [dir v] ({[0 1] [-1 0] [-1 0] [0 -1] [0 -1] [1 0] [1 0] [0 1]}
                          (if (zero? v) dir (mapv * dir (repeat -1)))))
        in-chan (async/chan)
        out-chan (async/chan)
        show (fn [b] (doseq [y (range 0 -6 -1)
                             :let [xs (range 43)
                                   l (s/join (map #({1 \#} (b [% y]) " ") xs))]]
                       (println l)))]
    (async/take! (run-intcode-machine in-chan out-chan state) (fn [x] (prn "Program:" x)))
    (loop [board {[0 0] (dec part)} dir [0 1] pos [0 0]]
      (async/put! in-chan (board pos 0))
      (let [[col rot] (async/<!! (async/into [] (async/take 2 out-chan)))]
        (if col
          (let [new-dir (turn dir rot)]
            (recur (assoc board pos col) new-dir (mapv + pos new-dir)))
          (case part
            1 (count board)
            2 (show board)))))))

(defn day12 [part input]
  (let [initial-positions (util/parse-lines
                            (fn [l] (mapv #(Long/parseLong %) (re-seq #"-?\d+" l)))
                            input)
        initial-state (zipmap (range) (mapv #(hash-map :r % :v [0 0 0]) initial-positions))
        pairs (comb/combinations (range (count initial-positions)) 2)
        update-r (fn [state]
                   (reduce-kv (fn [acc i m]
                                (assoc acc i (update m :r (partial mapv +) (:v m)))) {} state))
        update-v (fn [state [i1 i2]]
                   (let [dv (mapv compare (get-in state [i2 :r]) (get-in state [i1 :r]))]
                     (-> state
                         (update-in [i1 :v] (partial mapv +) dv)
                         (update-in [i2 :v] (partial mapv -) dv))))
        step (fn [state] (update-r (reduce update-v state pairs)))
        l1-norm (fn [v] (transduce (map #(Math/abs (long %))) + v))
        total-energy (fn [state]
                       (reduce-kv (fn [acc _ m]
                                    (+ acc (* (l1-norm (:r m)) (l1-norm (:v m))))) 0 state))]
    (case part
      1 (let [x (nth (iterate step initial-state) 1000)]
          [x (total-energy x)])
      2 (let [steps (iterate step initial-state)
              find-period (fn [f] (let [preamble (mapv f (take 10 steps))]
                                    (loop [steps (drop 1 steps) n 1]
                                      (if (< n 500000)
                                        (if (= (mapv f (take 10 steps)) preamble)
                                          n
                                          (recur (rest steps) (inc n)))
                                        :no-period-found))))
              lcm (fn [xs] (reduce (fn [a b] (/ (* a b) (first (util/extended-gcd a b)))) xs))
              periods-result (for [m (range (count initial-positions))
                                   t [:r :v]
                                   :let [p (mapv #(find-period (fn [st] (get-in st [m t %]))) [0 1 2])]]
                               [[m t] p])]
          (prn "Periods:")
          (doseq [r periods-result] (println r))
          (lcm (peek (first periods-result)))))))

(defn day13 [part input]
  (let [program (util/parse-nums input)
        out-chan (async/chan 2970)
        state (long-array (into program (repeat 1000 0)))]
    (case part
      1 (do (async/take!
              (run-intcode-machine nil out-chan state) (fn [x] (prn "Program stopped:" x)))
            (let [outputs (async/<!! (async/into [] out-chan))]
              (get (frequencies (into [] (comp (partition-all 3)
                                               (map peek)) outputs)) 2)))
      2 (let [_ (aset state 0 2)
              in-chan (async/chan)
              bi (img/new-image 43 23)
              score (atom 0)
              ball (atom nil)
              paddle (atom nil)
              update-img (fn [outputs]
                           (let [parts (into [] (partition-all 3) outputs)]
                             (doseq [[x y v] parts]
                               (when (= v 3) (reset! paddle x))
                               (when (= v 4) (reset! ball x))
                               (if (= [x y] [-1 0])
                                 (reset! score v)
                                 (img/set-pixel bi x y ({0 0xFF000000
                                                         1 0xFFFFFFFF
                                                         2 0xFFCCCCCC
                                                         3 0xFFFF0000
                                                         4 0xFF0000FF} v))))
                             (img/show bi :resize 20 :title (str "Score: " @score))))
              joystick-move (fn [] (let [x-paddle @paddle
                                         x-ball @ball]
                                     (long (compare x-ball x-paddle))))]
          (async/take!
            (run-intcode-machine in-chan out-chan state) (fn [x]
                                                           (prn "Program stopped:" x)
                                                           (async/close! in-chan)))
          (async/go-loop [outs []]
            (when (empty? outs) (async/<! (async/timeout 80)))
            (if-some [v (async/poll! out-chan)]
              (recur (conj outs v))
              (do
                (update-img outs)
                (async/put! in-chan (joystick-move))
                (recur []))))))))

(defn day14 [part input]
  (let [reactions (into {}
                        (util/parse-lines
                          (fn [l]
                            (let [eq (re-seq #"(\d+) (\w+)" l)
                                  [_ n x] (last eq)]
                              [x {:amount (Long/parseLong n)
                                  :reactants (into {}
                                                   (map (fn [tuple]
                                                          (let [[_ n x] tuple]
                                                            [x (Long/parseLong n)]))) (drop-last eq))}])) input))
        scale-fuel (fn [n] (update reactions "FUEL"
                                   (fn [m]
                                     {:amount n
                                      :reactants (reduce-kv (fn [acc k v]
                                                              (assoc acc k (* v n))) {} (:reactants m))})))
        find-ore-amount (fn [n]
                          (let [reactions (scale-fuel n)
                                required (get-in reactions ["FUEL" :reactants])]
                            (loop [required required stash {} used 0]
                              (if (seq required)
                                (let [[x n] (first required)]
                                  (if (reactions x)
                                    (let [x-per-tx (get-in reactions [x :amount])
                                          reactants (get-in reactions [x :reactants])
                                          num-tx (max (long (Math/ceil (/ (- n (stash x 0)) x-per-tx))) 0)
                                          x-created (* num-tx x-per-tx)]
                                      (recur
                                        (reduce-kv (fn [acc k v]
                                                     (update acc k (fnil + 0) (* num-tx v)))
                                                   (dissoc required x)
                                                   reactants)
                                        (update stash x (fnil + 0) (- x-created n))
                                        used))
                                    (recur (dissoc required x) stash (+ used n))))
                                used))))]
    (case part
      1 (find-ore-amount 1)
      2 (let [bisect (fn [f target]
                       (loop [a 1 b 1e9]
                         (if (< a b)
                           (let [x (long (+ (/ (- b a) 2) a))]
                             (if (> (f x) target)
                               (recur a x)
                               (recur (inc x) b)))
                           (dec a))))]
          (bisect find-ore-amount 1e12)))))

(defn day15 [input]
  (let [program (util/parse-nums input)
        state (long-array (into program (repeat 1000 0)))
        in-chan (async/chan)
        out-chan (async/chan)
        to-cmd (fn [dir] ({[0 1] 1 [0 -1] 2 [-1 0] 3 [1 0] 4} dir))]
    (async/take! (run-intcode-machine in-chan out-chan state) (fn [v] (prn v)))
    (let [grid (async/<!! (async/go-loop [m {[0 0] 1} path [[0 0]]]
                            (let [pos (peek path)
                                  [cmd new-pos] (first (for [dir [[0 1] [1 0] [-1 0] [0 -1]]
                                                             :let [new-pos (mapv + pos dir)]
                                                             :when (not (contains? m new-pos))]
                                                         [(to-cmd dir) new-pos]))]
                              (cond
                                cmd
                                (do
                                  (async/>! in-chan cmd)
                                  (when-let [v (async/<! out-chan)]
                                    (case v
                                      0 (recur (assoc m new-pos v) path)
                                      (1 2) (recur (assoc m new-pos v) (conj path new-pos)))))

                                (> (count path) 1)
                                (let [prev-path (pop path)
                                      prev-pos (peek prev-path)
                                      cmd (to-cmd (mapv - prev-pos pos))]
                                  (async/>! in-chan cmd)
                                  (when-let [_ (async/<! out-chan)]
                                    (recur m prev-path)))

                                :else (do (async/close! in-chan) m)))))
          tank-coords ((set/map-invert grid) 2)
          neighbours (fn [grid cur] (for [dir [[0 1] [1 0] [-1 0] [0 -1]]
                                          :let [p (mapv + cur dir)]
                                          :when (#{1 2} (grid p))] p))]
      {:part-1
       (let [path (util/a*-search [0 0] tank-coords (partial neighbours grid) (constantly 1) (constantly 0))]
         (dec (count path)))

       :part-2
       (loop [grid grid boundary #{tank-coords} n 0]
         (let [neighs (into #{} (mapcat (partial neighbours grid)) boundary)]
           (if (seq neighs)
             (recur (apply dissoc grid neighs) neighs (inc n))
             n)))})))

(defn day16 [part input]
  (let [signal (mapv #(Character/digit (char %) 10) (s/trim input))]
    (case part
      1 (let [pattern (fn [n] (rest (cycle (sequence (comp (map (partial repeat n)) cat) [0 1 0 -1]))))
              pattern-m (fn [n] (mapv #(into [] (take n) (pattern %)) (range 1 (inc n))))
              last-digit (fn [^long x] (rem (Math/abs x) 10))
              dot (fn [v w] (last-digit (reduce + (mapv * v w))))
              mmul (fn [m v] (mapv (partial dot v) m))]
          (apply str (take 8 (nth (iterate (partial mmul (pattern-m (count signal))) signal) 100))))
      2 (let [phase (fn [^ints arr] (loop [n (- (count arr) 2)]
                                      (when (>= n 0)
                                        (aset arr n (rem (+ (aget arr n) (aget arr (inc n))) 10))
                                        (recur (dec n)))))
              offset (Integer/parseInt (subs input 0 7))]
          (let [m (- (* 10000 (count signal)) offset)
                left (rem m (count signal))
                arr (int-array (take m (concat (take-last left signal) (cycle signal))))]
            (dotimes [_ 100] (phase arr)) (apply str (take 8 arr)))))))

(defn day17 [part input]
  (let [program (util/parse-nums input)
        state (long-array (into program (repeat 5000 0)))
        in-chan (async/chan)
        out-chan (async/chan)
        run (fn [in-chan]
              (async/take! (run-intcode-machine in-chan out-chan state) prn)
              (async/<!! (async/into [] out-chan)))]
    (case part
      1 (let [smap (s/split-lines (apply str (map char (run nil))))
              grid (into {} (for [[y l] (map vector (range) smap)
                                  [x ch] (map vector (range) l)]
                              [[x y] ch]))
              intersections (for [[[x y] ch] grid
                                  :let [neighbours [[(inc x) y] [(dec x) y] [x (inc y)] [x (dec y)]]]
                                  :when (and (= ch \#) (> (count (filter (comp #{\#} grid) neighbours)) 2))]
                              [x y])]
          (reduce (fn [acc [x y]] (+ acc (* x y))) 0 intersections))
      2
      ;; Complete route
      ;; R,12,L,6,R,12,L,8,L,6,L,10,R,12,L,6,R,12,R,12,L,10,L,6,R,10,L,8,L,6,L,10,R,12,L,10,
      ;; L,6,R,10,L,8,L,6,L,10,R,12,L,10,L,6,R,10,R,12,L,6,R,12,R,12,L,10,L,6,R,10

      ;; A - R,12,L,6,R,12
      ;; B - R,12,L,10,L,6,R,10
      ;; C - L,8,L,6,L,10
      ;; A,C,A,B,C,B,C,B,A,B
      (let [main-routine "A,C,A,B,C,B,C,B,A,B"
            fn-A "R,12,L,6,R,12"
            fn-B "R,12,L,10,L,6,R,10"
            fn-C "L,8,L,6,L,10"
            to-inputs (fn [s] (conj (mapv long s) 10))]
        (async/onto-chan in-chan (mapcat to-inputs [main-routine fn-A fn-B fn-C "n"]))
        (aset state 0 2)
        (peek (run in-chan))))))

(defn day18 [part input]
  (let [[grid inv-grid entrances] (let [g (util/parse-grid input)
                                        inv-grid (set/map-invert g)
                                        entrance (inv-grid \@)]
                                    (case part
                                      1 [g inv-grid [entrance]]
                                      2 (let [entrances (mapv (fn [d] (mapv + entrance d)) [[-1 -1] [1 -1] [-1 1] [1 1]])
                                              g (as-> g g
                                                      (reduce #(assoc % (mapv + entrance %2) \#) g [[0 1] [1 0] [-1 0] [0 -1] [0 0]])
                                                      (reduce #(assoc % %2 \@) g entrances))]
                                          [g (set/map-invert g) entrances])))
        find-keys (fn [pos]
                    (loop [q (conj PersistentQueue/EMPTY [pos 0 []]) leafs {} visited #{}]
                      (if (seq q)
                        (let [[node d doors] (peek q)
                              nnodes (for [dir [[0 1] [1 0] [-1 0] [0 -1]]
                                           :let [npos (mapv + node dir)]
                                           :when (not (visited npos))
                                           :let [k (grid npos)]
                                           :when (not (contains? #{\# nil} k))
                                           :let [ndoors (cond-> [] (<= 65 (int k) 90) (conj k))]]
                                       [npos (inc d) (into doors ndoors)])
                              ks (cond-> [] (<= 97 (int (grid node)) 122) (conj [node {:dist d :doors doors}]))]
                          (recur (into (pop q) nnodes) (into leafs ks) (conj visited node)))
                        leafs)))
        key-distances (into {} (map (juxt identity find-keys) (concat (keep inv-grid (map char (range 97 123))) entrances)))
        next-nodes (fn [pos ckeys]
                     (let [doors (into #{} (map #(Character/toUpperCase (char %))) ckeys)]
                       (into [] (comp
                                  (remove (comp ckeys grid key))
                                  (filter (comp #(set/subset? % doors) :doors val))
                                  (map (fn [[p v]] [p (:dist v)]))) (key-distances pos))))
        shortest-path (let [sp (fn [mem-sp poss ckeys]
                                 (let [shortest-path (partial mem-sp mem-sp) ; trick to memoize local recursive fn
                                       nnodes (for [i (range (count poss))
                                                    [p v] (next-nodes (poss i) ckeys)]
                                                [i p v])]
                                   (if (seq nnodes)
                                     (apply min (map (fn [[i p d]]
                                                       (+ d (shortest-path (assoc poss i p) (conj ckeys (grid p)))))
                                                     nnodes))
                                     0)))]
                        (partial sp (memoize sp)))]
    (shortest-path entrances #{})))

(defn day19 [part input]
  (let [program (util/parse-nums input)
        new-state #(long-array (into program (repeat 1000 0)))
        probe (fn [[x y]]
                (let [in-chan (async/chan)
                      out-chan (async/chan)]
                  (async/take! (run-intcode-machine in-chan out-chan (new-state)) (fn [v] (assert (= v :halt) (str "Not halt:" v))))
                  (async/<!! (async/go (async/>! in-chan x)
                                       (async/>! in-chan y)
                                       (async/<! out-chan)))))
        get-beam-width (fn get-beam-width [prev-l prev-u y]
                         (lazy-seq
                           (let [[a b] [(probe [prev-l y]) (probe [(inc prev-u) y])]
                                 l (if (zero? a) (inc prev-l) prev-l)
                                 u (if (zero? b) prev-u (inc prev-u))]
                             (if (<= l u)
                               (cons [y [l u]] (get-beam-width l u (inc y)))
                               (get-beam-width prev-l prev-u (inc y))))))]
    (case part
      1 (let [beam-points (take-while #(< (first %) 50) (get-beam-width 5 5 6))]
          (reduce (fn [total [_ [l u]]] (+ total 1 (- u l))) 1 beam-points))
      2 (let [find-square (fn [[y1 [l1 u1]] [y2 [l2 u2]]]
                            (let [min-x (max l1 l2)
                                  max-x (min u1 u2)
                                  w (inc (- max-x min-x))]
                              w))]
          (let [beam-points (get-beam-width 5 5 6) ; inspect to find start of nonzero width beam
                parts (partition 2 (interleave beam-points (drop 99 beam-points)))
                [[y [l1]] [_ [l2]]] (first (drop-while #(< (find-square (first %) (last %)) 100) parts))
                x (max l1 l2)]
            (+ (* x 10000) y))))))

(defn day20 [part input]
  (let [grid (let [raw-grid (util/parse-grid input)]
               (into {}
                     (for [[[x y] ch] raw-grid
                           :when (#{\.} ch)
                           :let [portal (first (for [dir [[0 -1] [1 0] [0 1] [-1 0]]
                                                     :let [[p1 p2 p3] (take 3 (rest (iterate (partial mapv + dir) [x y])))]
                                                     :when (<= 65 (int (raw-grid p1)) 90)
                                                     :let [outer? (nil? (raw-grid p3))
                                                           [p1 p2] (sort [p1 p2]) ;ugh - read label parts in correct order
                                                           label (str (raw-grid p1) (raw-grid p2) (when-not outer? \'))]]
                                                 (symbol label)))]]
                       [[x y] (or portal ch)])))
        inner-node? (fn [k] (s/ends-with? k "'"))
        inv-grid (set/map-invert grid)
        add-warp (fn [leafs] (reduce-kv (fn [acc k v]
                                          (if (zero? v)
                                            (let [w (symbol (s/replace (str k "'") "''" ""))]
                                              (if (inv-grid w) (assoc acc w 1) acc))
                                            (assoc acc k v))) {} leafs))
        distances (fn [k]
                    (loop [q (conj PersistentQueue/EMPTY [(inv-grid k) 0]) visited #{} leafs {}]
                      (if (seq q)
                        (let [[node d] (peek q)
                              nnodes (for [dir [[0 1] [1 0] [-1 0] [0 -1]]
                                           :let [npos (mapv + node dir)]
                                           :when (and (not (visited npos)) (grid npos))]
                                       [npos (inc d)])
                              new-leafs (cond-> [] (symbol? (grid node)) (conj [(grid node) d]))]
                          (recur (into (pop q) nnodes) (conj visited node) (into leafs new-leafs)))
                        (add-warp leafs))))
        dmap (into {} (map (juxt identity distances)) (filter symbol? (vals grid)))
        path-length (fn [path] (second (reduce (fn [[pos t] node] [node (+ t (node (dmap pos)))]) [(first path) 0] (rest path))))]
    (case part
      1 (let [path (util/a*-search 'AA 'ZZ (comp keys dmap) (fn [a b] (get-in dmap [a b])))]
          (path-length path))
      2 (let [neighbours (fn [[node lvl]]
                           (let [nn (dmap node)
                                 inode? (inner-node? node)]
                             (into [] (comp (map (fn [[k d]] (if (= d 1) ; warp to other level
                                                               [k (if inode? (inc lvl) (dec lvl))]
                                                               [k lvl])))
                                            (filter (comp nat-int? peek))) nn)))
              path (util/a*-search ['AA 0] ['ZZ 0] neighbours (fn [[a] [b]] (get-in dmap [a b])))]
          (path-length (map first path))))))

(comment

  (day11 1 (read-input 11))
  (day11 2 (read-input 11))

  (day12 1 (read-input 12))
  (day12 2 (read-input 12))

  (day13 1 (read-input 13))
  (day13 2 (read-input 13))

  (day14 1 (read-input 14))
  (day14 2 (read-input 14))

  (day15 (read-input 15))

  (day16 1 (read-input 16))
  (day16 2 (read-input 16))

  (day17 1 (read-input 17))
  (day17 2 (read-input 17))

  (day18 1 (read-input 18))
  (day18 2 (read-input 18))

  (day19 1 (read-input 19))
  (day19 2 (read-input 19))

  (day20 1 (read-input 20))
  (day20 2 (read-input 20))

  )

(defn day21 [part input]
  (let [program (util/parse-nums input)
        state (long-array (into program (repeat 1000 0)))
        in-chan (async/chan)
        out-chan (async/chan)
        to-inputs (fn [s] (conj (mapv long s) 10))
        run-script (fn [script]
                     (do (async/onto-chan in-chan (mapcat to-inputs script))
                         (async/take! (run-intcode-machine in-chan out-chan state) (fn [_]))
                         (let [output (async/<!! (async/into [] out-chan))]
                           (if (> (peek output) 255)
                             (peek output)
                             (println (s/join (map char output)))))))]
    (case part
      1 (run-script ["OR A J"
                     "AND B J"
                     "AND C J"
                     "NOT J J" ; Jump if there's a hole at A, B or C

                     "AND D J" ; But don't jump into a hole

                     "WALK"])
      2 (run-script ["OR A J"
                     "AND B J"
                     "AND C J"
                     "NOT J J"
                     "AND D J" ; Previous

                     "AND H J" ; Don't jump if H has hole
                     "NOT A T"
                     "OR T J" ; Unless A has a hole

                     "RUN"]))))

;; Explanation
;; Clearly the problem involves modular arithmetic, module the stack-size N.
;; Each shuffle command is a permutation of the indices. The permutation can be represented as a function
;; telling where the position of index x ends up:
;; new  : x -> N - x - 1 = -x - 1 (mod N)
;; cut a: x -> x - a (mod N)
;; inc a: x -> ax (mod N)
;; Each of these have the polynomial form of degree one: ax + b.
;; Performing two shuffles in succession yields a new polynomial of degree one.
;; (ax+b)c + d = (ac)x + bc + d
;; For part one, we can compute the final polynomial and eval: f(x) = ax + b
;; For part two, to perform the shuffle process M times, we exponentiate the polynomial.
;; The pattern of successive applications is:
;; ax + b => a^2x + ab + b => a^3x + a^2b + ab + b => etc
;; => (a^M)x + (a^M-1 + a^M-2 + ... + 1)b = (a^M)x + (a^M-1)(a-1)^-1 b
;; We can either take the inverse of this (equate with 2020 and solve for x) or find the inverse of each shuffle
;; I took the latter approach and created an inverse-command fn.
(defn day22 [part input]
  (let [N (case part 1 10007 (biginteger 119315717514047))
        commands (util/parse-lines (fn [l]
                                     (let [m (re-find #"increment|new|cut" l)]
                                       (case m
                                         "increment" [:inc (Long/parseLong (re-find #"-?\d+$" l))]
                                         "cut" [:cut (Long/parseLong (re-find #"-?\d+$" l))]
                                         "new" [:new]))) input)
        command-coef (fn [[op a]] (case op
                                    :new [-1 -1]
                                    :cut [1 (- a)]
                                    :inc [a 0]))
        mul-coef (fn [[a1 b1] [a2 b2]] [(mod (* a1 a2) N) (mod (+ (* b1 a2) b2) N)])
        pow-coef (fn [[a b] m]
                   (let [am (.modPow (biginteger a) (biginteger m) N)
                         amii (.modInverse (biginteger (dec a)) N)]
                     [am (mod (* (dec am) amii b) N)]))
        eval-poly (fn [[a b] x] (mod (+ (* a x) b) N))
        mod-inv (fn [a] (mod (second (util/extended-gcd a N)) N))
        inverse-command (fn [[op a]] (case op
                                       :new [op]
                                       :cut [op (- a)]
                                       :inc [op (mod-inv a)]))]
    (case part
      1 (eval-poly (reduce mul-coef [1 0] (map command-coef commands)) 2019)
      2 (let [M (biginteger 101741582076661)
              [a b] (reduce mul-coef [1N 0N] (map command-coef (map inverse-command (rseq commands))))]
          (long (eval-poly (pow-coef [a b] M) 2020))))))

(defn day23 [input]
  (let [program (util/parse-nums input)
        new-state #(long-array (into program (repeat 5000 0)))
        run-computer (fn [n in out]
                       (async/put! in n)
                       (async/take! (run-intcode-machine in out (new-state)) (fn [_])))
        in-chans (mapv (fn [_] (async/chan 100)) (range 50))
        out-chans (mapv (fn [_] (async/chan 100)) (range 50))
        package-bus (async/chan 1)]
    (dotimes [n 50] (run-computer n (in-chans n) (out-chans n)))
    ;; Deliver packages to bus - stops if package bus is closed
    (dotimes [n 50]
      (let [out-chan (out-chans n)]
        (async/go-loop []
          (let [[a x y] [(async/<! out-chan) (async/<! out-chan) (async/<! out-chan)]]
            (when (async/>! package-bus [a x y])
              (recur))))))
    ;; Package bus loop. Delivers packages or -1 to each input chan
    (async/<!!
      (async/go-loop [nat nil idle-counter 0 sent [] done? false]
        (if-not done?
          (if (= (count sent) 2)
            (let [[[_ y1] [_ y2]] sent]
              (println "Last 2 NAT packages sent:" sent)
              (if (= y1 y2)
                (recur nat idle-counter sent true)
                (recur nat idle-counter [(peek sent)] false)))
            (if-some [[a x y] (async/poll! package-bus)]
              (if (= a 255)
                (do
                  (when-not nat
                    (prn "First NAT package received:" [x y]))
                  (recur [x y] 0 sent done?))
                (do
                  (async/>! (in-chans a) x)
                  (async/>! (in-chans a) y)
                  (recur nat 0 sent done?)))
              (if (> idle-counter 50)
                (do
                  (when nat
                    (async/>! (in-chans 0) (first nat))
                    (async/>! (in-chans 0) (second nat)))
                  (recur nat 0 (conj sent nat) done?))
                (do (doseq [c in-chans]
                      (async/>! c -1))
                    (async/<! (async/timeout 10))
                    (recur nat (inc idle-counter) sent done?)))))
          ;; Cleanup
          (do
            (async/close! package-bus)
            (doseq [c in-chans] (async/close! c))
            sent))))))

(defn day24 [part input]
  (let [grid (into #{} (comp (filter (comp #{\#} val)) (map key)) (util/parse-grid input))
        neighbours-1 (fn [[x y]] (into [] (filter (fn [[x y]] (and (<= 0 x 4) (<= 0 y 4))))
                                       [[x (inc y)] [(inc x) y] [x (dec y)] [(dec x) y]]))
        rec-neighbours (memoize (fn [[x y]]
                                  (let [prev-lvl-points (cond-> #{}
                                                          (zero? x) (conj [-1 1 2])
                                                          (= x 4) (conj [-1 3 2])
                                                          (zero? y) (conj [-1 2 1])
                                                          (= y 4) (conj [-1 2 3]))
                                        next-lvl-points (case [x y]
                                                          [2 1] (mapv (fn [x] [1 x 0]) (range 5))
                                                          [2 3] (mapv (fn [x] [1 x 4]) (range 5))
                                                          [1 2] (mapv (fn [y] [1 0 y]) (range 5))
                                                          [3 2] (mapv (fn [y] [1 4 y]) (range 5))
                                                          [])
                                        same-lvl-points (into [] (filter (fn [[_ x y]] (and (<= 0 x 4) (<= 0 y 4) (not= x y 2))))
                                                              [[0 x (inc y)] [0 (inc x) y] [0 x (dec y)] [0 (dec x) y]])]
                                    (reduce into prev-lvl-points [same-lvl-points next-lvl-points]))))
        neighbours-2 (fn [[lvl x y]]
                       (into #{} (map (fn [x] (update x 0 + lvl))) (rec-neighbours [x y])))
        neighbours (memoize ({1 neighbours-1 2 neighbours-2} part))
        step (fn [state]
               (let [to-visit (into state (mapcat neighbours) state)
                     keep-fn (fn [node] (let [nbs (neighbours node)
                                              num-adj (count (keep state nbs))
                                              bug? (or (and (state node) (= num-adj 1))
                                                       (and (not (state node)) (#{1 2} num-adj)))]
                                          (when bug? node)))]
                 (into #{} (keep keep-fn) to-visit)))
        bd-rating (fn [g] (transduce (map (fn [[x y]] (let [n (+ (* y 5) x)] (long (Math/pow 2 n))))) + g))]
    (case part
      1 (loop [[g & gs] (iterate step grid) seen #{} n 0]
          (if (seen g) (bd-rating g) (recur gs (conj seen g) (inc n))))
      2 (let [grid-with-lvl (into #{} (map (partial into [0])) grid)]
          (count (nth (iterate step grid-with-lvl) 200))))))

(defn day25 [input]
  (let [program (util/parse-nums input)
        new-state #(long-array (into program (repeat 5000 0)))
        run-computer (fn [in out]
                       (async/take! (run-intcode-machine in out (new-state)) (fn [v]
                                                                               (prn "Computer:" v))))
        in-chan (async/chan)
        out-chan (async/chan)
        to-inputs (fn [s] (conj (mapv long s) 10))
        enter-command (fn [s]
                        (let [ips (to-inputs s)]
                          (async/onto-chan in-chan ips false)))]
    (run-computer in-chan out-chan)
    (async/<!!
      (async/go-loop [commands (into [;"west" "west" "west"
                                      ;"take hologram"
                                      ;"east" "east" "east" ;; back to start with hologram
                                      "south" "take fixed point" "north" ;; back with fixed point
                                      "north" "take candy cane"
                                      "west" #_"take antenna"
                                      ;"south" "take whirled peas" "north"
                                      "west" "take shell" "east" "east" ;; back with 2 more items
                                      "north" "north" "take polygon" "south"
                                      "west" #_"take fuel cell" "west" "west"]) outs []]
        (if (= (take-last 9 outs) [\C \o \m \m \a \n \d \? \newline])
          (let [_ (println (s/join outs))
                cmd (or (first commands) (read-line))
                _ (async/<! (enter-command cmd))]
            (recur (rest commands) []))
          (if-some [x (async/<! out-chan)]
            (recur commands (conj outs (char x)))
            (println (s/join outs))))))))
(comment

  (day21 1 (read-input 21))
  (day21 2 (read-input 21))

  (day22 1 (read-input 22))
  (day22 2 (read-input 22))

  (day23 (read-input 23)) ; 21160, 14327

  (day24 1 (read-input 24))
  (day24 2 (read-input 24))

  (day25 (read-input 25))

  ;; Contraints
  ;; FP required
  ;; Need HO or PO
  ;; Try without HO
  ;; - Need Polygon
  ;; - Need Candy Cane (all - HO - CC is too light) (4 left)
  ;; - Need to drop Antenna (dropping SH+WP+FC still too heavy)
  ;; - With shell gives solution
  )