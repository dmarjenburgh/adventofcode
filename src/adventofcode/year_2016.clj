(ns adventofcode.year-2016
  (:require [adventofcode.util :as util]
            [clojure.string :as s]
            [clojure.data.avl :as avl]
            [clojure.math.combinatorics :as combo]
            [clojure.set :as set])
  (:import [clojure.lang PersistentQueue]))

(def read-input (partial util/read-input 2016))

(defn- parse-int-rows [input]
  (let [lines (s/split-lines input)]
    (mapv (fn [l]
            (mapv #(Long/parseLong %) (re-seq #"\d+" l))) lines)))

(defn- parse-input1 [text]
  (->> (s/split text #",\s+")
       (map #(re-matches #"([LR])(\d+)\n?" %))
       (map (fn [[_ turn distance]]
              [({"L" :left
                 "R" :right} turn) (Long/parseLong distance)]))))

(defn breadth-first-search [starting-node neighbours]
  (iterate (fn [{:keys [nodes visited]}]
             (let [next-nodes (into #{}
                                    (comp
                                      (mapcat neighbours)
                                      (remove visited))
                                    nodes)]
               {:nodes next-nodes
                :visited (into visited next-nodes)})) {:nodes #{starting-node}
                                                       :visited #{starting-node}}))

(defn day1-1 [input]
  (let [dirs [:n :e :s :w]
        dir-fn (fn [dir turn]
                 (-> (.indexOf dirs dir)
                     (+ (if (= :left turn) -1 1))
                     (mod 4)
                     dirs))
        move (fn [[pos dir] [turn distance]]
               (let [next-dir (dir-fn dir turn)]
                 [(case next-dir
                    :n (update pos 1 + distance)
                    :e (update pos 0 + distance)
                    :s (update pos 1 - distance)
                    :w (update pos 0 - distance)) next-dir]))
        moves (parse-input1 input)
        [ending-pos ending-dir] (reduce move [[0 0] :n] moves)]
    (+ (Math/abs (first ending-pos)) (Math/abs (second ending-pos)))))

(defn day1-2 [input]
  (let [dirs [:n :e :s :w]
        dir-fn (fn [dir turn]
                 (-> (.indexOf dirs dir)
                     (+ (if (= :left turn) -1 1))
                     (mod 4)
                     dirs))
        positions (fn [[[x1 y1] d :as p1] [[x2 y2] d2 :as p2]]
                    (cond
                      (< x1 x2) (mapv (fn [x] [[x y1] d2]) (next (range x1 (inc x2))))
                      (> x1 x2) (mapv (fn [x] [[x y1] d2]) (next (range x1 (dec x2) -1)))
                      (< y1 y2) (mapv (fn [y] [[x1 y] d2]) (next (range y1 (inc y2))))
                      (> y1 y2) (mapv (fn [y] [[x1 y] d2]) (next (range y1 (dec y2) -1)))))
        move (fn [[pos dir] [turn distance]]
               (let [next-dir (dir-fn dir turn)]
                 [(case next-dir
                    :n (update pos 1 + distance)
                    :e (update pos 0 + distance)
                    :s (update pos 1 - distance)
                    :w (update pos 0 - distance)) next-dir]))
        moves (parse-input1 input)
        coords (map first (reduce (fn [acc m]
                                    (into acc (positions (peek acc) (move (peek acc) m)))) [[[0 0] :n]] moves))
        [x y] (loop [[pos & more] coords visited #{}]
                (if (contains? visited pos)
                  pos
                  (recur more (conj visited pos))))]
    (+ (Math/abs x) (Math/abs y))))

(defn day2-1 [input]
  (let [instructions (->> (s/split-lines input)
                          (mapv #(re-seq #"\w" %)))
        move (fn [[x y] dir]
               (cond
                 (and (= "U" dir) (pos? y)) [x (dec y)]
                 (and (= "R" dir) (< x 2)) [(inc x) y]
                 (and (= "D" dir) (< y 2)) [x (inc y)]
                 (and (= "L" dir) (pos? x)) [(dec x) y]
                 :else [x y]))
        do-moves (fn [pos moves]
                   (reduce move pos moves))
        pos->num (fn [[x y]] (+ (* y 3) x 1))]
    (->> (reduce (fn [acc moves]
                   (conj acc (do-moves (peek acc) moves))) [[1 1]] instructions)
         (next)
         (map pos->num)
         (apply str))))

(defn day2-2 [input]
  (let [instructions (->> (s/split-lines input)
                          (mapv #(re-seq #"\w" %)))
        dist (fn [[x1 y1] [x2 y2]] (+ (Math/abs (- x1 x2)) (Math/abs (- y1 y2))))
        move (fn [[x y] dir]
               (let [new-pos (case dir
                               "U" [x (dec y)]
                               "R" [(inc x) y]
                               "D" [x (inc y)]
                               "L" [(dec x) y])]
                 (if (> (dist new-pos [2 2]) 2)
                   [x y]
                   new-pos)))
        do-moves (fn [pos moves]
                   (reduce move pos moves))
        pos->num (fn [[x y]]
                   (cond
                     (zero? y) (+ 1 (- x 2))
                     (= y 1) (+ 2 (- x 1))
                     (= y 2) (+ 5 (- x 0))
                     (= y 3) (+ 10 (- x 1))
                     (= y 4) (+ 13 (- x 2))))]
    (->> (reduce (fn [acc moves]
                   (conj acc (do-moves (peek acc) moves))) [[0 2]] instructions)
         (next)
         (map pos->num)
         (map #(Long/toHexString %))
         (apply str))))

(defn day3-1 [input]
  (let [triangles (parse-int-rows input)
        valid-triangle? (fn [t] (let [[a b c] (sort t)]
                                  (> (+ a b) c)))]
    (count (filter valid-triangle? triangles))))

(defn day3-2 [input]
  (let [input (parse-int-rows input)
        transformed (partition 3 (apply mapcat vector input))
        valid-triangle? (fn [t] (let [[a b c] (sort t)]
                                  (> (+ a b) c)))]
    (count (filter valid-triangle? transformed))))

(defn- parse-room [line]
  (let [[_ a serial-num checksum] (re-matches #"([a-z-]+)+(\d+)\[([a-z]+)\]\n?" line)]
    (zipmap [:names :sector-id :checksum] [(s/split a #"-") (Long/parseLong serial-num) checksum])))

(defn- real-room? [{:keys [names checksum]}]
  (let [letters (distinct (apply str names))
        letter-frequencies (frequencies (apply str names))
        sort-fn #(- (letter-frequencies %))]
    (->> (sort-by (juxt sort-fn identity) letters)
         (take 5)
         (apply str)
         (= checksum))))

(defn day4-1 [input]
  (let [rooms (mapv parse-room (s/split-lines input))]
    (->> rooms
         (filter real-room?)
         (map :sector-id)
         (apply +))))

(defn- decrypt [{:keys [names sector-id]}]
  (let [abc "abcdefghijklmnopqrstuvwxyz"
        shifted (->> (cycle abc)
                     (drop sector-id)
                     (take 26))
        lookup (zipmap abc shifted)]
    (mapv (fn [n] (apply str (mapv lookup n))) names)))

(defn day4-2 [input]
  (let [rooms (mapv parse-room (s/split-lines input))]
    (->> rooms
         (filter #(= ["northpole" "object" "storage"]
                     (decrypt %))) ; Find by searching for north in decrypted room names
         first
         :sector-id)))

(defn day5 [day input]
  (let [valid-positions #{\0 \1 \2 \3 \4 \5 \6 \7}]
    (if (= 1 day)
      (loop [n 0 result ""]
        (if (< (count result) 8)
          (let [md5-string (util/md5 (str input n))]
            (if (s/starts-with? md5-string "00000")
              (recur (inc n) (str result (nth md5-string 5)))
              (recur (inc n) result)))
          result))
      (loop [n 0 result {}]
        (if (< (count result) 8)
          (let [md5-string (util/md5 (str input n))
                position (nth md5-string 5)]
            (if (s/starts-with? md5-string "00000")
              (recur (inc n) (if (or (contains? result position)
                                     (not (contains? valid-positions position)))
                               result
                               (assoc result position (nth md5-string 6))))
              (recur (inc n) result)))
          (->> (into (sorted-map) result) vals (apply str)))))))

(defn day6 [day input]
  (let [lines (s/split-lines input)
        columns (apply mapv vector lines)
        letter (if (= 1 day)
                 (fn [coll] (key (apply max-key val (frequencies coll))))
                 (fn [coll] (key (apply min-key val (frequencies coll)))))]
    (apply str (map letter columns))))

(defn- parse-ips [input]
  (let [lines (s/split-lines input)
        parse-ip (fn [line] (loop [l line acc {}]
                              (if-let [m (re-find #"^\w+" l)]
                                (recur (subs l (count m)) (update acc :supernet-sequences (fnil conj []) m))
                                (if-let [[fm m] (re-find #"^\[(\w+)\]" l)]
                                  (recur (subs l (count fm)) (update acc :hypernet-sequences (fnil conj []) m))
                                  acc))))]
    (mapv parse-ip lines)))
(defn day7-1 [input]
  (let [ips (parse-ips input)
        has-abba? (fn [s] (let [ms (re-seq #"(\w)(\w)\2\1" s)]
                            (and (seq ms)
                                 (not-any? (fn [[m]] (= 1 (count (set m)))) ms))))
        supports-tls? (fn [ip] (and
                                 (not-any? has-abba? (:hypernet-sequences ip))
                                 (some has-abba? (:supernet-sequences ip))))]
    (count (filterv supports-tls? ips))))

(defn day7-2 [input]
  (let [ips (parse-ips input)
        aba (fn [s] (let [ms (re-seq #"(?=((\w)(\w)\2))" s)]
                      (vec (keep (fn [[_ m]] (when (> (count (set m)) 1)
                                               m)) ms))))
        supports-sls? (fn [ip]
                        (let [abas (mapcat aba (:supernet-sequences ip))]
                          (some (fn [aba]
                                  (let [bab (str (subs aba 1) (second aba))
                                        hs (:hypernet-sequences ip)]
                                    (boolean (some #(s/includes? % bab) hs)))) abas)))]
    (count (filterv supports-sls? ips))))

(defn- parse-instructions [input]
  (let [lines (s/split-lines input)
        parse-instruction (fn [line]
                            (cond
                              (s/starts-with? line "rect") (let [[_ w h] (re-find #"(\d+)x(\d+)" line)]
                                                             [:rect (Long/parseLong w) (Long/parseLong h)])
                              (s/starts-with? line "rotate row") (let [[_ r n] (re-find #"y=(\d+) by (\d+)" line)]
                                                                   [:rotate-row (Long/parseLong r) (Long/parseLong n)])
                              (s/starts-with? line "rotate column") (let [[_ c n] (re-find #"x=(\d+) by (\d+)" line)]
                                                                      [:rotate-column (Long/parseLong c) (Long/parseLong n)])))]
    (mapv parse-instruction lines)))
(defn day8 [day input]
  (let [instructions (parse-instructions input)
        turn-on (fn [state [row col]] (assoc-in state [row col] 1))
        rotate-r (fn [state row n]
                   (reduce (fn [acc i] (assoc-in acc [row i] (get-in state [row (mod (- i n) 50)])))
                           state
                           (range 50)))
        rotate-c (fn [state col n]
                   (reduce (fn [acc i] (assoc-in acc [i col] (get-in state [(mod (- i n) 6) col]))) state (range 6)))
        apply-instruction (fn [state [instruction a b]]
                            (case instruction
                              :rect (reduce turn-on state (for [col (range a) row (range b)] [row col]))
                              :rotate-row (rotate-r state a b)
                              :rotate-column (rotate-c state a b)))
        count (fn [matrix]
                (transduce (comp (mapcat identity)
                                 (filter #{1})) + matrix))
        show (fn [matrix] (doseq [row matrix]
                            (doseq [col row]
                              (print (if (zero? col) "  " "# ")))
                            (println)))]
    (->> (reduce apply-instruction (vec (repeat 6 (vec (repeat 50 0)))) instructions)
         ((if (= day 1)
            count
            show)))))

(defn day9
  ([day text]
   (day9 day text 0))
  ([day text result]
   (loop [text text result 0]
     (if-let [[_ pre marker post] (re-find #"([A-Z]*)\((\d+x\d+)\)(.*)" text)]
       (let [[num-chars n] (map #(Long/parseLong %) (next (re-find #"(\d+)x(\d+)" marker)))]
         (if (= day 1)
           (recur (subs post num-chars) (+ result (count pre) (* n (count (subs post 0 num-chars)))))
           (recur (subs post num-chars) (+ result (count pre) (* n (day9 day (subs post 0 num-chars) result))))))
       (+ (count text) result)))))

(defn day10 [day input]
  (let [lines (s/split-lines input)
        instructions (mapv (fn [line]
                             (if-let [[m a b] (re-find #"value (\d+) goes to bot (\d+)" line)]
                               [:input (Long/parseLong a) (Long/parseLong b)]
                               (when-let [[m b lo ln ho hn] (re-find #"bot (\d+) gives low to (\w+) (\d+) and high to (\w+) (\d+)" line)]
                                 [:give (Long/parseLong b) [(keyword lo) (Long/parseLong ln)] [(keyword ho) (Long/parseLong hn)]]))) lines)
        give (fn [state [_ bot [lo ln] [ho hn]]]
               (let [bot-state (get-in state [:bot bot])]
                 (when (= (count bot-state) 2)
                   (cond-> state
                     (= lo :output) (assoc-in [:output ln] (apply min bot-state))
                     (= lo :bot) (update-in [:bot ln] (fnil conj #{}) (apply min bot-state))
                     (= ho :output) (assoc-in [:output hn] (apply max bot-state))
                     (= ho :bot) (update-in [:bot hn] (fnil conj #{}) (apply max bot-state))
                     :always (assoc-in [:bot bot] #{})))))
        handle-inst (fn [data]
                      (let [{:keys [state instructions]} data
                            inst (first instructions)]
                        (if inst
                          (case (first inst)
                            :input (-> data
                                       (update-in [:state :bot (nth inst 2)] (fnil conj #{}) (nth inst 1))
                                       (update-in [:instructions] subvec 1))
                            :give (if-let [new-state (give state inst)]
                                    (-> data
                                        (assoc :state new-state)
                                        (update :instructions subvec 1))
                                    (update data :instructions #(conj (subvec % 1) inst))))
                          data)))
        states (->> (iterate handle-inst {:state {} :instructions instructions})
                    (take-while (comp seq :instructions)))]
    (if (= day 1)
      (->> states
           (keep (fn [state]
                   (when-let [bot (first (keep (fn [x] (when (#{#{17 61}} (val x))
                                                         (key x))) (get-in state [:state :bot])))]
                     bot)))
           (first))
      (apply * (vals (select-keys (get-in (last states) [:state :output]) [0 1 2]))))))

(comment
  (day1-1 (read-input 1))
  (day1-2 (read-input 1))

  (day2-1 (read-input 2))
  (day2-2 (read-input 2))

  (day3-1 (read-input 3))
  (day3-2 (read-input 3))

  (day4-1 (read-input 4))
  (day4-2 (read-input 4))

  (day5 1 "uqwqemis")
  (day5 2 "uqwqemis")

  (day6 1 (read-input 6))
  (day6 2 (read-input 6))

  (day7-1 (read-input 7))
  (day7-2 (read-input 7))

  (day8 1 (read-input 8))
  (day8 2 (read-input 8))

  (day9 1 (read-input 9))
  (day9 2 (read-input 9))

  (day10 1 (read-input 10))
  (day10 2 (read-input 10))
  )

(defn day11
  ([input] (day11 input #{}))
  ([input extra-stuff]
   (let [parse-part (fn [p]
                      (cond
                        (s/includes? p "generator")
                        [(keyword (first (s/split p #" "))) :generator]
                        (s/includes? p "microchip")
                        [(keyword (first (s/split p #"-"))) :microchip]))
         parse (fn [line]
                 (let [[_ line] (s/split line #"contains ")
                       parts (s/split line #", and|,|and")]
                   (when (first parts)
                     (into #{}
                           (comp
                             (map #(re-find #"(\w+ generator|\w+-compatible microchip|nothing relevant)" %))
                             (map second)
                             (map parse-part)
                             (filter some?))
                           parts))))
         valid-floor? (fn [items]
                        (let [{chips :microchip generators :generator :as freqs} (group-by second items)]
                          (or (empty? generators)
                              (every? #(contains? items [(first %) :generator]) chips))))
         initial-layout (update (mapv parse (s/split-lines input)) 0 into extra-stuff)
         initial-state {:current-floor 0 :layout initial-layout}
         show (fn [state]
                (let [items (sort (reduce into #{} (:layout state)))]
                  (doseq [[e floor] (reverse (map vector (range) (:layout state)))
                          :let [_ (print (str "\nF" e "\t"))
                                _ (print (if (= (:current-floor state) e) "E" ".") \tab)]
                          [el t :as item] items]
                    (print (if (contains? floor item) (s/upper-case (str (first (name el)) (first (name t)))) ".") \tab))
                  (println)))
         end-state? (fn [states]
                      (first
                        (keep (fn [{:keys [current-floor layout] :as s}]
                                (when (and (= 3 current-floor)
                                           (every? empty? (take 3 layout)))
                                  s))
                              states)))
         neighbours (fn [{:keys [current-floor layout]}]
                      (let [floors (remove #{-1 4} [(dec current-floor) (inc current-floor)])
                            items (layout current-floor)
                            to-take (into (mapv vector items) (combo/combinations items 2))]
                        (for [f floors
                              itms to-take
                              :let [ncur-floor (apply disj items itms)
                                    nfloor (into (layout f) itms)]
                              :when (and (valid-floor? ncur-floor)
                                         (valid-floor? nfloor))]
                          {:current-floor f :layout (assoc layout
                                                      current-floor ncur-floor
                                                      f nfloor)})))]
     (loop [current-states #{initial-state} visited #{initial-state} n 0]
       (if (end-state? current-states)
         n
         (let [nstates (into #{} (comp (mapcat neighbours) (remove visited)) current-states)]
           (recur nstates (into visited nstates) (inc n))))))))

(defn day12 [part input]
  (let [reg {:a 0 :b 1 :c 2 :d 3}
        program (mapv (fn [line]
                        (let [[inst x y] (s/split line #" ")]
                          (condp = inst
                            "cpy" (if (re-matches #"[abcd]" x)
                                    [:cpy-r (reg (keyword x)) (reg (keyword y))]
                                    [:cpy-v (Long/parseLong x) (reg (keyword y))])
                            "inc" [:inc (reg (keyword x))]
                            "dec" [:dec (reg (keyword x))]
                            "jnz" (if (re-matches #"[abcd]" x)
                                    [:jnz-r (reg (keyword x)) (Long/parseLong y)]
                                    [:jnz-v (Long/parseLong x) (Long/parseLong y)])))) (s/split-lines input))
        state (if (= part 1) (long-array 4) (doto (long-array 4) (aset 2 1)))
        op! (fn ^long [^longs state ^long pc]
              (let [[inst ^long x ^long y] (get program pc)]
                (case inst
                  :cpy-r (do (aset state y (aget state x)) (inc pc))
                  :cpy-v (do (aset state y x) (inc pc))
                  :inc (do (aset state x (inc (aget state x))) (inc pc))
                  :dec (do (aset state x (dec (aget state x))) (inc pc))
                  :jnz-r (if (zero? (aget state x)) (inc pc) (+ pc y))
                  :jnz-v (if (zero? x) (inc pc) (+ pc y)))))
        max-pc (long (count program))]
    (loop [pc (long 0)]
      (if (>= pc max-pc)
        (first state)
        (let [npc (long (op! state pc))]
          (recur npc))))))

(defn day13 [day input target]
  (let [bit-count (fn [x] (Long/bitCount x))
        open? (fn [[x y]]
                (let [sum (+ input (* x x) (* 3 x) (* 2 x y) y (* y y))]
                  (even? (bit-count sum))))
        neighbours (fn [pos]
                     (for [d [[1 0] [0 1] [-1 0] [0 -1]]
                           :let [[x y :as new-pos] (mapv + pos d)]
                           :when (and (>= x 0)
                                      (>= y 0)
                                      (open? new-pos))]
                       new-pos))
        cost-fn (constantly 1)
        manhattan-distance (fn [[x1 y1] [x2 y2]]
                             (+ (Math/abs (- x2 x1))
                                (Math/abs (- y2 y1))))]
    (if (= day 1)
      (dec (count (util/a*-search [1 1] target neighbours cost-fn manhattan-distance)))
      (count (:visited (nth (breadth-first-search [1 1] neighbours) 50))))))

(defn day14 [part input]
  (let [hashes (if (= part 1)
                 (map #(util/md5 (str input %)) (range))
                 (map (fn [i] (nth (iterate util/md5 (str input i)) 2017)) (range)))
        is-key (fn [index char]
                 (let [re (re-pattern (apply str (repeat 5 char)))]
                   (some (partial re-find re) (take 1000 (drop (inc index) hashes)))))]
    (nth (for [index (range)
               :let [hash (nth hashes index)
                     [_ match] (re-find #"(\w)\1\1" hash)]
               :when (and match
                          (is-key index match))]
           [index match]) 63)))

(defn day15 [input]
  (let [parsed-input (mapv (fn [line]
                             (mapv #(Long/parseLong %) (next (re-matches #"Disc #(\d+) has (\d+) positions.* at position (\d+)\." line))))
                           (s/split-lines input))
        admissible-ts (fn [[d num-pos start]]
                        (mod (- 0 d start) num-pos))]
    (util/chinese-remainder (mapv second parsed-input) (mapv admissible-ts parsed-input))))

(defn day16 [input length]
  (let [dragon-step (fn [a]
                      (let [b (map {\1 \0 \0 \1} (reverse a))]
                        (apply str a \0 b)))
        data (subs (first (drop-while #(< (count %) length) (iterate dragon-step input))) 0 length)
        checksum (fn [s]
                   (if (even? (count s))
                     (let [parts (partition 2 s)]
                       (recur (apply str (map (fn [[a b]]
                                                (if (= a b) "1" "0")) parts))))
                     s))]
    (checksum data)))

(defn day17 [day input]
  (let [door-state (memoize #(if (contains? #{\b \c \d \e \f} %) :open :closed))
        doors (fn [path]
                (let [hash (subs (util/md5 (str input path)) 0 4)]
                  (mapv door-state hash)))
        neighbours (fn [{:keys [pos path]}]
                     (when (not= pos [4 4])
                       (for [[dir state] (zipmap ["U" "D" "L" "R"] (doors path))
                             :when (and (= state :open))
                             :let [[x y :as new-pos] (mapv + pos ({"U" [0 -1] "D" [0 1]
                                                                   "L" [-1 0] "R" [1 0]} dir))]
                             :when (and (pos? x) (pos? y))]
                         {:pos new-pos
                          :path (str path dir)})))
        paths (mapcat :nodes (breadth-first-search {:pos [1 1] :path ""} neighbours))]
    (if (= day 1)
      (first (filter (comp #{[4 4]} :pos) paths))
      (filter (comp #{[4 4]} :pos) paths))))

(defn day18 [day input]
  (let [step (fn [s]
               (apply str
                      (for [[l c r] (partition 3 1 [\. \.] (cons \. s))]
                        (case [l c r]
                          ([\^ \. \.] [\. \. \^] [\^ \^ \.] [\. \^ \^]) \^
                          \.))))]
    (->> (take (if (= day 1) 40 400000) (iterate step (s/trim input)))
         (s/join)
         (frequencies))))

(defn day19 [day input]
  (let [initial-state [1 (into (avl/sorted-map) (for [i (range 1 (inc input))] [i 1]))]
        step (fn [[elf state]]
               (let [[before current after] (avl/split-key elf state)
                     after (concat (take 2 after) [(first state) (second state)])
                     [robbed-elf num-presents] (first after)
                     [next-elf] (second after)]
                 [next-elf (-> state
                               (update elf + num-presents)
                               (dissoc robbed-elf))]))
        step2 (fn [[elf state]]
                (if (> (count state) 1)
                  (let [[before] (avl/split-key elf state)
                        current-index (count before)
                        num-elfs (count state)
                        robbed-index (mod (+ current-index (int (/ num-elfs 2))) num-elfs)
                        [robbed-elf num-presents] (first (second (avl/split-at robbed-index state)))
                        new-state (-> state (update elf + num-presents) (dissoc robbed-elf))
                        next-index (mod (inc (count (first (avl/split-key elf new-state)))) (dec num-elfs))
                        next-elf (ffirst (second (avl/split-at next-index new-state)))]
                    [next-elf new-state])
                  [nil state]))]
    (if (= day 1)
      (last (take-while first (iterate step initial-state)))
      (last (take-while first (iterate step2 initial-state))))))

(defn day20 [input]
  (let [ranges (mapv (fn [line]
                       (let [[_ start end] (re-matches #"(\d+)-(\d+)" line)]
                         [(Long/parseLong start) (Long/parseLong end)])) (s/split-lines input))]
    (loop [n 0 allowed-ips []]
      (if (<= n 4294967295)
        (if-let [r (some (fn [range]
                           (when (<= (first range) n (second range))
                             range)) ranges)]
          (recur (inc (second r)) allowed-ips)
          (recur (inc n) (conj allowed-ips n)))
        [(apply min allowed-ips) (count allowed-ips)]))))

(comment

  (day11 (read-input 11))
  (day11 (read-input 11) #{[:elerium :generator]
                           [:elerium :microchip]
                           [:dilithium :generator]
                           [:dilithium :microchip]}) ;; 61  - Very slow

  (day12 1 (read-input 12))
  (day12 2 (read-input 12))

  (day13 1 1362 [31 39])
  (day13 2 1362 [31 39])

  (day14 1 "jlmsuwbz")
  (day14 2 "jlmsuwbz")

  (day15 (read-input 15))
  (day15 (str (read-input 15) "Disc #7 has 11 positions; at time=0, it is at position 0.\n"))

  (day16 "10111100110001111" 272)
  (day16 "10111100110001111" 35651584)

  (day17 1 "bwnlcvfs")
  (day17 2 "ihgpwlah") ; very slow

  (day18 1 (read-input 18))
  (day18 2 (read-input 18)) ; slow

  (day19 1 3001330) ; slow [1808357 {1808357 3001330}]
  (day19 2 3001330) ; very slow [1407007 {1407007 3001330}]

  (day20 (read-input 20))
  )

(defn day21 [day input]
  (let [operations (mapv (fn [line]
                           (cond
                             (s/starts-with? line "swap position")
                             (let [[_ a b] (re-find #"position (\d+) with position (\d+)" line)]
                               [:swap-position (Long/parseLong a) (Long/parseLong b)])
                             (s/starts-with? line "swap letter")
                             (let [[_ a b] (re-find #"letter (\w) with letter (\w)" line)]
                               [:swap-letter a b])
                             (s/starts-with? line "rotate right")
                             (let [[_ a] (re-find #"(\d+) step" line)]
                               [:rotate-right (Long/parseLong a)])
                             (s/starts-with? line "rotate left")
                             (let [[_ a] (re-find #"(\d+) step" line)]
                               [:rotate-left (Long/parseLong a)])
                             (s/starts-with? line "rotate based")
                             (let [[_ a] (re-find #"position of letter (\w)" line)]
                               [:rotate-based-on a])
                             (s/starts-with? line "reverse positions")
                             (let [[_ a b] (re-find #"positions (\d+) through (\d+)" line)]
                               [:reverse-positions (Long/parseLong a) (Long/parseLong b)])
                             (s/starts-with? line "move position")
                             (let [[_ a b] (re-find #"position (\d+) to position (\d+)" line)]
                               [:move-position (Long/parseLong a) (Long/parseLong b)])))
                         (s/split-lines input))
        insert (fn [s i c]
                 (apply str (subs s 0 i) c (subs s (inc i))))
        rotate-r (fn [s] (apply str (last s) (subs s 0 (dec (count s)))))
        rotate-l (fn [s] (str (subs s 1) (first s)))
        do-op (fn do-op [s op]
                (case (first op)
                  :swap-position (let [[_ i j] op
                                       li (nth s i)
                                       lj (nth s j)]
                                   (-> s (insert i lj) (insert j li)))
                  :swap-letter (let [[_ a b] op]
                                 (s/replace s (re-pattern (str a "|" b)) (fn [m]
                                                                           (if (= m a) b a))))
                  :rotate-right (let [[_ n] op]
                                  (nth (iterate rotate-r s) n))
                  :rotate-left (let [[_ n] op]
                                 (nth (iterate rotate-l s) n))
                  :rotate-based-on (let [[_ a] op
                                         index (.indexOf s a)]
                                     (do-op s [:rotate-right (+ (inc index) (if (>= index 4) 1 0))]))
                  :reverse-positions (let [[_ a b] op
                                           start (min a b)
                                           end (inc (max a b))]
                                       (str (subs s 0 start) (apply str (reverse (subs s start end))) (subs s end)))
                  :move-position (let [[_ i j] op
                                       a (nth s i)
                                       s1 (str (subs s 0 i) (subs s (inc i)))]
                                   (str (subs s1 0 j) a (subs s1 j)))))]
    (if (= day 1)
      (reduce do-op "abcdefgh" operations)
      (first
        (for [perm (combo/permutations "abcdefgh")
              :when (= (reduce do-op (apply str perm) operations) "fbgdceah")]
          (apply str perm))))))

(defn day22 [part input]
  (let [nodes (mapv (fn [line]
                      (let [[_ x y size used avail] (re-find #"node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)" line)]
                        {:node [(Long/parseLong x) (Long/parseLong y)]
                         :size (Long/parseLong size)
                         :used (Long/parseLong used)
                         :avail (Long/parseLong avail)})) (drop 2 (s/split-lines input)))]
    (if (= part 1)
      (count (for [pair (combo/combinations nodes 2)
                   order [identity reverse]
                   :let [[a b] (order pair)]
                   :when (and
                           (not (zero? (:used a)))
                           (<= (:used a) (:avail b)))]
               1))
      (let [nodes (into {} (map (juxt :node identity)) nodes)
            show (fn [] (dorun
                          (for [y (range 35)
                                :let [_ (print \newline)]
                                x (range 30)
                                :let [{size :size used :used} (nodes [x y])
                                      ch (cond
                                           (= [x y] [0 0]) \T
                                           (= [x y] [29 0]) \G
                                           (zero? used) \_
                                           (>= used 100) \#
                                           :else \.)]]
                            (print ch)))
                   (println))]
        ;; Go left 5, up 34, right 6. Then repeat d,l,l,u,r 28 times
        (show)
        (+ 5 34 6 (* 5 28)))))) ;; 185

(defn day23 [part input]
  (let [reg {:a 0 :b 1 :c 2 :d 3}
        program (into-array
                  (mapv (fn [line]
                          (let [[inst x y] (s/split line #" ")]
                            (condp = inst
                              "cpy" (if (re-matches #"[abcd]" x)
                                      [:cpy-r (reg (keyword x)) (reg (keyword y))]
                                      [:cpy-v (Long/parseLong x) (reg (keyword y))])
                              "inc" [:inc (reg (keyword x))]
                              "dec" [:dec (reg (keyword x))]
                              "jnz" (if (re-matches #"[abcd]" x)
                                      (if (re-matches #"[abcd]" y)
                                        [:jnz-rr (reg (keyword x)) (reg (keyword y))]
                                        [:jnz-rv (reg (keyword x)) (Long/parseLong y)])
                                      (if (re-matches #"[abcd]" y)
                                        [:jnz-vr (Long/parseLong x) (reg (keyword y))]
                                        [:jnz-vv (Long/parseLong x) (Long/parseLong y)]))
                              "tgl" [:tgl (reg (keyword x))]))) (s/split-lines input)))
        state (long-array [(if (= part 1) 7 12) 0 0 0])
        max-pc (long (alength program))
        op! (fn ^long [^longs state ^long pc]
              (let [[inst ^long x ^long y] (aget program pc)]
                (if (= pc 4) ;; shortcut it
                  (let [[a b c d] (vec state)]
                    (aset state 0 (+ a (* b d)))
                    (aset state 2 0)
                    (aset state 3 0)
                    (+ pc 5))
                  (case inst
                    :cpy-r (do (aset state y (aget state x)) (inc pc))
                    :cpy-v (do (aset state y x) (inc pc))
                    :inc (do (aset state x (inc (aget state x))) (inc pc))
                    :dec (do (aset state x (dec (aget state x))) (inc pc))
                    :jnz-rr (if (zero? (aget state x)) (inc pc) (+ pc (aget state y)))
                    :jnz-rv (if (zero? (aget state x)) (inc pc) (+ pc y))
                    :jnz-vr (if (zero? x) (inc pc) (+ pc (aget state y)))
                    :jnz-vv (if (zero? x) (inc pc) (+ pc y))
                    :tgl (let [i (+ pc (aget state x))
                               [tinst tx ty] (when (< i max-pc) (aget program i))]
                           (when tinst
                             (aset program i [(case tinst
                                                :inc :dec
                                                :dec :inc
                                                :cpy-r :jnz-rr
                                                :cpy-v :jnz-vr
                                                :jnz-rr :cpy-r
                                                :jnz-rv :cpy-ir
                                                :jnz-vr :cpy-v
                                                :jnz-vv :cpy-iv
                                                :tgl :inc
                                                :cpy-ir :jnz-rv
                                                :cpy-iv :jnz-vv) tx ty]))
                           (inc pc))
                    (inc pc)))))]
    (loop [pc (long 0) n 0]
      (if (>= pc max-pc)
        (first state)
        (let [npc (long (op! state pc))]
          (recur npc (inc n)))))))

(defn bfs24 [start grid]
  (loop [stack (conj PersistentQueue/EMPTY [start 0]) visited #{start} leafs {}]
    (if (seq stack)
      (let [[node d] (peek stack)
            leaf? (number? (grid node))
            nnodes (for [dir [[0 1] [1 0] [0 -1] [-1 0]]
                         :let [pos (mapv + node dir)]
                         :when (and (grid pos)
                                    (not (visited pos)))]
                     pos)]
        (recur (into (pop stack) (map #(vector % (inc d))) nnodes)
               (into visited nnodes)
               (cond-> leafs
                 (and leaf? (pos? d)) (assoc (grid node) d))))
      leafs)))

(defn day24 [part input]
  (let [grid (into {} (remove (comp #{\#} val)) (util/parse-grid (fn [ch] (or (#{\. \#} ch) (Long/parseLong (str ch)))) input))
        inv-grid (set/map-invert grid)
        distances (into {} (map (juxt grid #(bfs24 % grid))) (map inv-grid (range 8)))]
    (apply min
           (for [comb (combo/permutations (range 1 8))
                 :let [path (partition 2 1 (cond-> (into [0] comb) (= part 2) (conj 0)))]]
             (reduce (fn [acc [a b]]
                       (+ acc (get-in distances [a b]))) 0 path)))))

(defn day25 [input]
  (let [reg {:a 0 :b 1 :c 2 :d 3}
        output (atom [])
        program (into-array
                  (mapv (fn [line]
                          (let [[inst x y] (s/split line #" ")]
                            (condp = inst
                              "cpy" (if (re-matches #"[abcd]" x)
                                      [:cpy-r (reg (keyword x)) (reg (keyword y))]
                                      [:cpy-v (Long/parseLong x) (reg (keyword y))])
                              "inc" [:inc (reg (keyword x))]
                              "dec" [:dec (reg (keyword x))]
                              "jnz" (if (re-matches #"[abcd]" x)
                                      (if (re-matches #"[abcd]" y)
                                        [:jnz-rr (reg (keyword x)) (reg (keyword y))]
                                        [:jnz-rv (reg (keyword x)) (Long/parseLong y)])
                                      (if (re-matches #"[abcd]" y)
                                        [:jnz-vr (Long/parseLong x) (reg (keyword y))]
                                        [:jnz-vv (Long/parseLong x) (Long/parseLong y)]))
                              "out" (if (re-matches #"[abcd]" x)
                                      [:out-r (reg (keyword x))]
                                      [:out-v (Long/parseLong x)]))))
                        (s/split-lines input)))
        state (long-array 4)
        max-pc (long (alength program))
        op! (fn ^long [^longs state ^long pc]
              (let [[inst ^long x ^long y] (aget program pc)]
                (if (= pc 2) ;shortcut
                  (let [c (aget state 2)
                        d (aget state 3)]
                    (aset state 3 (+ d (* 643 c)))
                    (aset state 2 0)
                    (aset state 1 0)
                    (+ pc 5))
                  (case inst
                    :cpy-r (do (aset state y (aget state x)) (inc pc))
                    :cpy-v (do (aset state y x) (inc pc))
                    :inc (do (aset state x (inc (aget state x))) (inc pc))
                    :dec (do (aset state x (dec (aget state x))) (inc pc))
                    :jnz-rr (if (zero? (aget state x)) (inc pc) (+ pc (aget state y)))
                    :jnz-rv (if (zero? (aget state x)) (inc pc) (+ pc y))
                    :jnz-vr (if (zero? x) (inc pc) (+ pc (aget state y)))
                    :jnz-vv (if (zero? x) (inc pc) (+ pc y))
                    :out-r (let [o (swap! output conj (aget state x))
                                 no (count o)]
                             (if (= o (take no (cycle [0 1])))
                               (inc pc)
                               max-pc))))))
        run-it (fn [a]
                 (doseq [i [1 2 3]] (aset state i 0))
                 (aset state 0 a)
                 (reset! output [])
                 (loop [pc (long 0) n 0]
                   (if (or (= n 50000) (>= pc max-pc))
                     @output
                     (let [npc (long (op! state pc))]
                       (recur npc (inc n))))))]
    (first
      (keep-indexed (fn [i n]
                      (let [o (run-it n)]
                        (when (= o (take (count o) (cycle [0 1])))
                          i)))
                    (range 200)))))

(comment

  (day21 1 (read-input 21))
  (day21 2 (read-input 21))

  (day22 1 (read-input 22))
  (day22 2 (read-input 22))

  (day23 1 (read-input 23))
  (day23 2 (read-input 23))

  (day24 1 (read-input 24))
  (day24 2 (read-input 24))

  (day25 (read-input 25))

  )
