(ns adventofcode.year-2017
  (:require [adventofcode.util :as util]
            [clojure.java.io :as io]
            [clojure.string :as s]
            [clojure.data.avl :as avl]
            [clojure.math.combinatorics :as combo]
            [clojure.set :as set])
  (:import [java.util LinkedList]
           [clojure.lang PersistentQueue]))

(defn read-resource [file]
  (slurp (io/resource file)))

(defn day1-1 [input]
  (->> (str input (first input))
       (partition-by identity)
       (mapcat next)
       (map #(Long/parseLong (str %)))
       (reduce +)))

(defn day1-2 [input]
  (let [nums (mapv #(Long/parseLong (str %)) input)
        step (/ (count nums) 2)]
    (reduce (fn [agg [i n]]
              (if (= n (nth nums (rem (+ i step) (count nums))))
                (+ agg n)
                agg))
            0
            (mapv vector (range) nums))))

(defn day2-1 [input]
  (let [matrix (->> (s/split-lines input)
                    (mapv (fn [line] (mapv #(Long/parseLong %) (s/split line #"\s+")))))]
    (reduce (fn [agg row]
              (+ agg (- (apply max row) (apply min row))))
            0
            matrix)))

(defn day2-2 [input]
  (let [matrix (->> (s/split-lines input)
                    (mapv (fn [line] (mapv #(Long/parseLong %) (s/split line #"\s+")))))
        row-fn (fn [nums]
                 (some (fn [[a b]]
                         (when (zero? (rem (max a b) (min a b)))
                           (/ (max a b) (min a b)))) (combo/combinations nums 2)))]
    (reduce (fn [agg row]
              (+ agg (row-fn row)))
            0
            matrix)))

(defn day3-1
  "The bottom-right number of the nxn square is an odd square"
  [input]
  ; sqrt(361527) -> 601 at [300, -300]
  ; 361527 - 601^2 = 326
  ; 1 right, 325 up -> [301, 25]
  (+ 301 25))

(defn day3-2 [input]
  (let [max-value (Long/parseLong input)
        deltas (fn [n] (concat (repeat n [1 0]) (repeat n [0 1]) (repeat (inc n) [-1 0]) (repeat (inc n) [0 -1])))
        next-value (fn [spiral [x y]]
                     (reduce + (for [dx [-1 0 1]
                                     dy [-1 0 1]
                                     :when (not= dx dy 0)
                                     :let [pos [(+ x dx) (+ y dy)]]
                                     :when (contains? spiral pos)]
                                 (get spiral pos))))]
    (loop [spiral {[0 0] 1} current-position [0 0] next-moves [] round 1]
      (if-let [move (first next-moves)]
        (let [next-position (mapv + current-position move)
              nv (next-value spiral next-position)]
          (if (> nv max-value)
            nv
            (recur (assoc spiral next-position nv) next-position (next next-moves) round)))
        (recur spiral current-position (deltas round) (+ round 2))))))

(defn day4-1 [input]
  (let [phrases (mapv #(s/split % #"\s+") (s/split-lines input))]
    (count (filter #(apply distinct? %) phrases))))

(defn day4-2 [input]
  (let [phrases (mapv #(s/split % #"\s+") (s/split-lines input))
        flip (fn [word] (apply str (reverse word)))
        valid-passphrase? (fn [words]
                            (= (count words) (count (into #{} (map frequencies) words))))]
    (count (filter valid-passphrase? phrases))))

(defn day5-1 [input]
  (let [instructions (mapv #(Long/parseLong %) (s/split input #"\s+"))
        do-op (fn [[i state]]
                (let [jump (get state i)]
                  (when (< -1 (+ i jump) (count instructions))
                    [(+ i jump) (update state i inc)])))]
    (count (take-while some? (iterate do-op [0 instructions])))))

(defn day5-2 [input]
  (let [instructions (mapv #(Long/parseLong %) (s/split input #"\s+"))
        do-op (fn [[i state]]
                (let [jump (get state i)]
                  (when (< -1 (+ i jump) (count instructions))
                    [(+ i jump) (update state i (fn [x] (if (>= x 3) (dec x) (inc x))))])))]
    (count (take-while some? (iterate do-op [0 instructions])))))

(defn day6-1 [input]
  (let [blocks (mapv #(Long/parseLong %) (s/split input #"\s+"))
        num-blocks (count blocks)
        redistribute (fn [blocks]
                       (let [max-val (apply max blocks)
                             block-index (.indexOf blocks max-val)]
                         (reduce (fn [agg i]
                                   (update agg (rem i num-blocks) inc)) (assoc blocks block-index 0) (range (inc block-index) (+ (inc block-index) max-val)))))]
    (loop [[state & more] (iterate redistribute blocks) visited #{} round 0]
      (if (contains? visited state)
        [round state]
        (recur more (conj visited state) (inc round))))))

(defn day6-2 [input]
  (let [recurring-state (second (day6-1 input))]
    (first (day6-1 (s/join "\t" recurring-state)))))

(defn day7-1 [input]
  (let [parse-line (fn [line]
                     (let [[_ name weight more] (re-find #"(\w+) \((\d+)\)( -> (.*+))?" line)]
                       {:name name
                        :weight (Long/parseLong weight)
                        :children (if more (s/split (subs more 4) #", ") [])}))
        parsed-input (mapv parse-line (s/split-lines input))]
    (set/difference (set (map :name parsed-input)) (into #{} (mapcat :children) parsed-input))))

(defn day7-2 [input]
  (let [parse-line (fn [line]
                     (let [[_ name weight more] (re-find #"(\w+) \((\d+)\)( -> (.*+))?" line)]
                       {:name name
                        :weight (Long/parseLong weight)
                        :children (if more (s/split (subs more 4) #", ") [])}))
        entities (mapv parse-line (s/split-lines input))
        root (first (day7-1 input))
        get-node (fn [name] (first (keep #(when (= (:name %) name) %) entities)))
        weight (fn weight [node-name]
                 (let [node (get-node node-name)
                       w (:weight node)]
                   (reduce + w (map weight (:children node)))))
        off-node (fn [node-name]
                   (let [ch (:children (get-node node-name))
                         weights (mapv (fn [n] [n (weight n)]) ch)]
                     (reduce (fn [agg [_ pairs]]
                               (if (and (= 1 (count pairs)) (not= 1 (count ch)))
                                 (reduced (ffirst pairs))
                                 agg))
                             nil
                             (group-by second weights))))]
    (mapv (fn [n] [n (weight n)]) (:children (get-node (last (butlast (take-while some? (iterate off-node root)))))))
    (map weight (:children (get-node "apjxafk")))))

(defn day8 [day input]
  (let [parse-line (fn [line]
                     (let [[_ reg cmd v c1 c2 c3 :as r] (re-find #"(\w+) (\w+) (-?\d+) if (\w+) (\S+) (-?\d+)" line)]
                       [reg (keyword cmd) (Long/parseLong v) [c1 (keyword c2) (Long/parseLong c3)]]))
        pred (fn [state [c1 c2 c3]]
               (({:== = :< < :> > :<= <= :>= >= :!= not=} c2) (get state c1 0) c3))
        program (mapv parse-line (s/split-lines input))
        do-op (fn [state [reg cmd v p]]
                (if (pred state p)
                  (update state reg (fnil ({:inc + :dec -} cmd) 0) v)
                  state))]
    (if (= day 1)
      (apply max (vals (reduce do-op {} program)))
      (apply max (mapcat vals (reductions do-op {} program))))))

(defn day9 [day input]
  (let [cleaned (loop [[c & more] input output "" comment? nil gar 0]
                  (if c
                    (cond
                      (= c \!) (recur (next more) output comment? gar)
                      (and (= c \<) (not comment?)) (recur more output true gar)
                      (and (= c \{) (not comment?)) (recur more (str output "[") comment? gar)
                      (and (= c \}) (not comment?)) (recur more (str output "]") comment? gar)
                      (and (= c \>) comment?) (recur more output false gar)
                      comment? (recur more output comment? (inc gar))
                      :else (recur more output comment? gar))
                    (if (= day 1) output gar)))]
    (if (= day 1)
      (let [nested-vecs (read-string cleaned)
            score (fn score [depth v]
                    (if (empty? v)
                      depth
                      (reduce + depth (map (partial score (inc depth)) v))))]
        (score 1 nested-vecs))
      cleaned)))

(defn day10-1 [input]
  (let [lengths (mapv #(Long/parseLong %) (s/split (s/trim input) #","))
        num-beads 256
        do-op (fn [[skip cur-pos beads] length]
                (let [sub-list (mapv #(nth beads (rem (+ cur-pos %) num-beads)) (range length))
                      rsb (vec (reverse sub-list))]
                  [(inc skip)
                   (rem (+ cur-pos length skip) num-beads)
                   (reduce (fn [agg i]
                             (assoc agg (rem (+ cur-pos i) num-beads) (nth rsb i))) beads (range length))]))
        end-state (peek (reduce do-op [0 0 (vec (range num-beads))] lengths))]
    (* (first end-state) (second end-state))))

(defn day10-2 [input]
  (let [input-lengths (mapv int (s/trim input))
        standard-suffix [17 31 73 47 23]
        lengths (into input-lengths standard-suffix)
        num-beads 256
        do-op (fn [[skip cur-pos beads] length]
                (let [sub-list (mapv #(nth beads (rem (+ cur-pos %) num-beads)) (range length))
                      rsb (vec (reverse sub-list))]
                  [(inc skip)
                   (rem (+ cur-pos length skip) num-beads)
                   (reduce (fn [agg i]
                             (assoc agg (rem (+ cur-pos i) num-beads) (nth rsb i))) beads (range length))]))
        end-state (loop [state [0 0 (vec (range num-beads))] iter 0]
                    (if (< iter 64)
                      (let [new-state (reduce do-op state lengths)]
                        (recur new-state (inc iter)))
                      (peek state)))]
    (->> (partition 16 end-state)
         (map (partial apply bit-xor))
         byte-array
         util/bytes-to-hex)))


(defn day11 [day input]
  (let [steps (mapv (comp keyword s/trim) (s/split input #","))
        dirs {:n [1 0] :ne [0 1] :se [-1 1] :s [-1 0] :sw [0 -1] :nw [1 -1]}
        move (fn [pos step]
               (mapv + pos (get dirs step)))]
    (if (= day 1)
      (reduce + (reduce move [0 0] steps))
      (apply max (map (partial reduce +) (reductions move [0 0] steps))))))

(defn dfs [starting-node next-nodes-fn]
  (loop [stack [starting-node] visited #{}]
    (if-let [node (peek stack)]
      (let [to-visit (remove visited (next-nodes-fn node))]
        (recur (into (pop stack) to-visit) (conj visited node)))
      visited)))

(defn connected-components [nodes next-nodes-fn]
  (loop [visited #{} cc 0 [node & more] nodes]
    (if node
      (if (contains? visited node)
        (recur visited cc more)
        (recur (into visited (dfs node next-nodes-fn)) (inc cc) more))
      cc)))

(defn day12 [day input]
  (let [parse-line (fn [line]
                     (let [[_ node-s neighbours-s] (re-find #"(\d+) <-> (.*)" line)]
                       [(Long/parseLong node-s) (into #{} (map #(Long/parseLong %)) (s/split neighbours-s #", "))]))
        graph (into {} (map parse-line) (s/split-lines input))
        next-nodes-fn (fn [node] (get graph node))]
    (if (= day 1)
      (count (dfs 0 next-nodes-fn))
      (connected-components (keys graph) graph))))

(defn day13-1 [input & [delay-t]]
  (let [delay-t (or delay-t 0)
        layers (into {} (map (fn [line]
                               (let [[_ depth r] (re-find #"(\d+): (\d+)" line)]
                                 [(Long/parseLong depth) (Long/parseLong r)]))) (s/split-lines input))
        initial-state (into {} (map (fn [[depth]] [depth 0])) layers)
        max-layer (apply max (keys layers))
        position (fn [^long r ^long t]
                   (let [mr (dec r)]
                     (- mr (Math/abs (long (- (rem t (* 2 mr)) mr))))))
        next-state-fn (fn [state t]
                        (into {} (map (fn [[d p]]
                                        [d (position (layers d) t)])) state))
        caught-locs (loop [state initial-state t 0 caught-at []]
                      (let [package-layer (- t delay-t 1)]
                        (if (< package-layer max-layer)
                          (let [next-t (inc t)
                                new-pl (inc package-layer)
                                caught? (zero? (get state new-pl -1))
                                new-state (next-state-fn state next-t)]
                            (recur new-state next-t (if caught? (conj caught-at new-pl) caught-at)))
                          caught-at)))]
    [caught-locs
     (reduce + (map * caught-locs (map layers caught-locs)))]))

(defn day13-2 [input]
  (let [layers (into [] (map (fn [line]
                               (let [[_ depth r] (re-find #"(\d+): (\d+)" line)]
                                 [(Long/parseLong depth) (Long/parseLong r)]))) (s/split-lines input))
        no-go-flter (fn [[d r]] (remove #(zero? (rem (+ % d) (- (* 2 r) 2)))))
        filters (apply comp (map no-go-flter layers))]
    (transduce filters (fn
                         ([x] x)
                         ([agg x] (reduced x))) false (range))))

(defn day14-1 [input]
  (let [inputs (map (partial str input "-") (range 128))
        knot-hashes (map day10-2 inputs)
        bs (map #(let [bs (.toString (BigInteger. ^String % 16) 2)
                       pad (apply str (repeat (- 128 (count bs)) "0"))]
                   (str pad bs)) knot-hashes)]
    (frequencies (apply str bs))))

(defn day14-2 [input]
  (let [xform-inputs (map (partial str input "-"))
        xform-knot-hashes (map day10-2)
        xform-bits (map #(let [bs (.toString (BigInteger. ^String % 16) 2)
                               pad (apply str (repeat (- 128 (count bs)) "0"))]
                           (str pad bs)))
        grid (into [] (comp xform-inputs xform-knot-hashes xform-bits) (range 128))
        neighbouring-pos (fn [cpos] (for [d [[-1 0] [0 1] [1 0] [0 -1]]
                                          :let [[x y :as pos] (mapv + cpos d)]
                                          :when (and (<= 0 x 128) (<= 0 y 128))]
                                      pos))
        graph (into {}
                    (for [r (range 128)
                          c (range 128)
                          :let [x (get-in grid [r c])]
                          :when (= x \1)
                          :let [np (neighbouring-pos [r c])
                                neighs (filterv (fn [pos] (= \1 (get-in grid pos))) np)]]
                      [[r c] neighs]))]
    (connected-components (keys graph) graph)))

(defn day15 [day input]
  (let [[start-a start-b] (map #(Long/parseLong %) (re-seq #"\d+" input))
        gen-a-factor 16807
        gen-b-factor 48271
        modulo 2147483647
        next-pair (fn [[a b]]
                    [(rem (* a gen-a-factor) modulo)
                     (rem (* b gen-b-factor) modulo)])
        match? (fn [[a b]]
                 (= (bit-and 0xffff a) (bit-and 0xffff b)))
        iter-a (iterate #(rem (* % gen-a-factor) modulo) start-a)
        iter-b (iterate #(rem (* % gen-b-factor) modulo) start-b)
        iter-a-2 (filter #(zero? (rem % 4)) iter-a)
        iter-b-2 (filter #(zero? (rem % 8)) iter-b)]
    (->> (map vector (if (= day 1) iter-a iter-a-2) (if (= day 1) iter-b iter-b-2))
         (next)
         (take (if (= day 1) 40000000 5000000))
         (filter match?)
         count)))

(defn day16 [day input]
  (let [parse-move (fn [s]
                     (case (first s)
                       \s [:spin (Long/parseLong (str (subs s 1)))]
                       \x (into [:exchange] (map #(Long/parseLong (str %)) (s/split (subs s 1) #"/")))
                       \p [:partner (second s) (last s)]))
        slength 16
        moves (mapv parse-move (s/split (s/trim input) #","))
        do-move (fn [^String state [move a b]]
                  (case move
                    :spin (subs (str (subs state (- slength a)) state) 0 slength)
                    :exchange (.toString (doto (StringBuilder. state)
                                           (.setCharAt a (get state b))
                                           (.setCharAt b (get state a))))
                    :partner (s/replace state (re-pattern (str \[ a b \])) {(str a) (str b)
                                                                            (str b) (str a)})))
        dance (fn [state] (reduce do-move state moves))
        initial-state (apply str (map char (range 97 (+ 97 slength))))]
    (if (= day 1)
      (dance initial-state)
      ;; Dance repeats after 60 iterations
      (-> (iterate dance (apply str (map char (range 97 (+ 97 slength)))))
          (nth (rem (int 1e9) 60))))))

(defn day17-1 [input]
  (let [steps (Long/parseLong (s/trim input))
        buffer (LinkedList. [0])
        move (fn [index ^LinkedList buffer]
               (let [n (.size buffer)
                     new-index (rem (+ index steps 1) (.size buffer))]
                 (.add buffer new-index n)
                 new-index))]
    (loop [index 0]
      (if (< (.size buffer) 2018)
        (recur (move index buffer))
        (seq buffer)))))

(defn day17-2 [input]
  (let [steps (Long/parseLong (s/trim input))
        move (fn [index buffer size]
               (let [new-index (rem (+ index steps 1) size)]
                 [new-index (if (zero? new-index) (conj buffer size) buffer) (inc size)]))]
    (loop [[index buffer size] [0 [] 1]]
      (if (< size 50000001)
        (recur (move index buffer size))
        (peek buffer)))))

(defn day18 [day input]
  (let [parse-instr (fn [line]
                      (let [[op a b] (s/split line #"\s+")]
                        (cond-> [(keyword op)
                                 (if (re-find #"\d+" a)
                                   (Long/parseLong a)
                                   (keyword a))]
                          b (conj (if (re-find #"\d+" b)
                                    (Long/parseLong b)
                                    (keyword b))))))
        program (mapv parse-instr (s/split-lines input))
        op-val (fn [state v] (if (number? v) v (get state v)))
        do-op (fn [state [op a b]]
                (let [state (case op
                              :snd (assoc state :sound (op-val state a))
                              :set (assoc state a (op-val state b))
                              :add (update state a + (op-val state b))
                              :mul (update state a * (op-val state b))
                              :mod (update state a mod (op-val state b))
                              :rcv (cond-> state
                                     (not (zero? (op-val state a))) (assoc
                                                                      a (:sound state)
                                                                      :recovered (:sound state)))
                              :jgz (if (> (op-val state a) 0)
                                     (update state :pc + (op-val state b))
                                     (update state :pc inc)))]
                  (cond-> state
                    (not= op :jgz) (update :pc inc))))
        initial-state (zipmap (into #{:pc} (comp
                                             (mapcat next)
                                             (filter keyword?)) program) (repeat 0))
        c (atom 0)]
    (if (= day 1)
      (loop [state initial-state]
        (if (:recovered state)
          state
          (if-let [instruction (get program (:pc state))]
            (recur (do-op state instruction))
            state)))
      (loop [[state1 state2] [initial-state (assoc initial-state :p 1)] turn 0 topics [PersistentQueue/EMPTY PersistentQueue/EMPTY] sends 0]
        (when (<= 1374 @c 1400) (prn @c state2 ((juxt state1 state2) :pc) (get program (:pc state2))))
        (let [state (if (zero? turn) state1 state2)]
          (if-let [instruction (get program (:pc state))]
            (cond
              (and (:waiting? state1) (:waiting? state2) (every? empty? topics))
              (do
                (prn "Deadlock reached")
                [state1 state2 sends])
              (= :snd (first instruction))
              (recur (update-in [state1 state2] [turn :pc] inc)
                     turn
                     (update topics turn conj (op-val state (second instruction)))
                     (if (zero? turn) sends (inc sends)))

              (= :rcv (first instruction))
              (if-let [v (peek (topics (- 1 turn)))]
                (recur (-> [state1 state2]
                           (assoc-in [turn (second instruction)] v)
                           (assoc-in [turn :waiting?] false)
                           (update-in [turn :pc] inc)) turn (update topics (- 1 turn) pop) sends)
                (recur (assoc-in [state1 state2] [turn :waiting?] true) (- 1 turn) topics sends))
              :else (recur (update [state1 state2] turn do-op instruction) turn topics sends))
            (do
              (prn "Program" (inc turn) "terminated")
              [state1 state2 sends])))))))

(defn day19 [input]
  (let [chart (mapv vec (s/split-lines input))
        initial-state [[1 0] [0 (.indexOf input "|")]]
        letters (atom "")
        move (fn [[dir pos]]
               (let [x (get-in chart pos)]
                 (case x
                   \| [dir (mapv + pos dir)]
                   \- [dir (mapv + pos dir)]
                   \+ (let [new-dirs (case dir
                                       [1 0] [[0 1] [0 -1]]
                                       [0 1] [[-1 0] [1 0]]
                                       [-1 0] [[0 1] [0 -1]]
                                       [0 -1] [[-1 0] [1 0]])
                            new-dir (first (for [d new-dirs
                                                 :let [v (get-in chart (mapv + pos d))]
                                                 :when (not= v \space)]
                                             d))]
                        [new-dir (mapv + pos new-dir)])
                   \space nil
                   (do
                     (swap! letters str x)
                     [dir (mapv + pos dir)]))))]
    [(dec (count (take-while some? (iterate move initial-state)))) @letters]))

(defn day20 [day input]
  (let [parse-line (fn [line] (into [] (comp
                                         (map #(Long/parseLong %))
                                         (partition-all 3)) (re-seq #"-?\d+" line)))
        initial-state (mapv parse-line (s/split-lines input))]
    (if (= day 1)
      (let [a2 (fn [[_ _ acc]] (reduce + (map #(* % %) acc)))
            smallest-acc (apply min-key a2 initial-state)]
        (.indexOf initial-state smallest-acc))
      (let [move (fn [[pos vel acc]] (let [nvel (mapv + vel acc)]
                                       [(mapv + pos nvel) nvel acc]))
            remove-collisions (fn [ps]
                                (let [leftover-pos (into #{} (keep (fn [[k v]] (when (= v 1) k))) (frequencies (mapv first ps)))]
                                  (into #{} (filter (comp leftover-pos first)) ps)))]
        (loop [particles initial-state t 0]
          (if (and (seq particles) (< t 10000))
            (let [np (remove-collisions (into [] (map move) particles))]
              (recur np (inc t)))
            (count particles)))))))

(defn day21 [input]
  (let [parse-rule (fn [rule]
                     (let [parts (s/split rule #"/")]
                       (mapv (partial mapv {\. 0 \# 1}) parts)))
        count-ons (fn [pattern] (mapv #(count (filter #{1} %)) pattern))
        total-ons (fn [pattern] (count (into [] (comp (mapcat identity)
                                                      (filter #{1})) pattern)))
        parse-line (fn [line]
                     (let [[in out] (s/split line #" => ")
                           in-rule (parse-rule in)]
                       {:in in-rule
                        :out (parse-rule out)
                        :total-ons (total-ons in-rule)}))
        rules (mapv parse-line (s/split-lines input))
        flip (fn [grid]
               (mapv (fn [r]
                       (mapv (fn [c]
                               (get-in grid [r c])) (range (dec (count grid)) -1 -1))) (range (count grid))))
        rot (fn [grid]
              (mapv (fn [c]
                      (mapv (fn [r]
                              (get-in grid [r c])) (range (dec (count grid)) -1 -1))) (range (count grid))))
        symmetries (fn [grid]
                     (for [f [identity flip]
                           g (next (take 5 (iterate rot (f grid))))]
                       g))
        starting-pattern [[0 1 0] [0 0 1] [1 1 1]]
        size-filter (fn [pattern]
                      (let [size (count pattern)]
                        (filter (fn [rule] (= (count (:in rule)) size)))))
        total-count-filter (fn [pattern]
                             (let [total-ons-count (total-ons pattern)]
                               (filter (fn [rule] (= (:total-ons rule) total-ons-count)))))
        symmetry-filter (fn [pattern]
                          (let [syms (symmetries pattern)]
                            (filter (fn [rule] (some #{(:in rule)} syms)))))
        emap (fn [f m] (mapv (fn [row]
                               (mapv f row)) m))
        match-rule (memoize
                     (fn [pattern]
                       (let [matches (into [] (comp
                                                (size-filter pattern)
                                                (total-count-filter pattern)
                                                (symmetry-filter pattern)) rules)]
                         (first matches))))
        split-pattern (fn [pattern]
                        (let [n (count pattern)
                              split-size (if (even? n) 2 3)
                              num-grids (quot n split-size)]
                          (into []
                                (partition-all split-size)
                                (vec (for [r (range 0 n split-size)
                                           c (range 0 n split-size)]
                                       (into [] (comp (map (partial get-in pattern))
                                                      (partition-all split-size)) (for [x (range split-size)
                                                                                        y (range split-size)]
                                                                                    [(+ x r) (+ y c)])))))))
        unsplit (fn [subgrids]
                  (let [num-grids (count subgrids)
                        grid-size (count (ffirst subgrids))]
                    (vec (for [r (range num-grids)
                               gr (range grid-size)]
                           (vec (for [c (range num-grids)
                                      gc (range grid-size)]
                                  (get-in subgrids [r c gr gc])))))))
        step (fn [grid]
               (let [split-grid (split-pattern grid)]
                 (unsplit (emap (comp :out match-rule) split-grid))))]
    (-> (iterate step starting-pattern)
        (nth 10)
        (total-ons))))

(defn day22-1 [input]
  (let [grid (into #{} (for [[i row] (map vector (range) (s/split-lines input))
                             [j node] (mapv vector (range) row)
                             :when (= node \#)]
                         [i j]))
        initial-state [[-1 0] [12 12] 0 grid]
        dirs [[-1 0] [0 1] [1 0] [0 -1]] ; n, e, s, w
        turn-right (zipmap dirs (next (cycle dirs)))
        turn-left (zipmap dirs (drop 3 (cycle dirs)))
        clean (fn [grid pos] (disj grid pos))
        infect (fn [grid pos] (conj grid pos))
        step (fn [[dir pos infections grid]]
               (let [infected? (contains? grid pos)
                     new-dir (if infected? (turn-right dir) (turn-left dir))]
                 (if infected?
                   [new-dir (mapv + pos new-dir) infections (clean grid pos)]
                   [new-dir (mapv + pos new-dir) (inc infections) (infect grid pos)])))
        print-state (fn [[dir pos _ grid]]
                      (prn dir)
                      (doseq [row (range -10 40)]
                        (prn)
                        (doseq [col (range -10 50)]
                          (print (if (contains? grid [row col])
                                   (if (= [row col] pos) \x \#)
                                   (if (= [row col] pos) \o \.)))))
                      (prn))]
    (print-state initial-state)
    (loop [state initial-state bursts 0]
      (if (< bursts 10000)
        (recur (step state) (inc bursts))
        (do (print-state state)
            (zipmap [:dir :pos :infections] state))))))

(defn day22-2 [input]
  (let [grid (into {} (for [[i row] (map vector (range) (s/split-lines input))
                            [j node] (mapv vector (range) row)
                            :when (= node \#)]
                        [[i j] :infected]))
        initial-state [[-1 0] [12 12] 0 grid]
        dirs [[-1 0] [0 1] [1 0] [0 -1]] ; n, e, s, w
        turn-right (zipmap dirs (next (cycle dirs)))
        turn-left (zipmap dirs (drop 3 (cycle dirs)))
        clean (fn [grid pos] (dissoc grid pos))
        step (fn [[dir pos infections grid]]
               (let [node (grid pos :clean)
                     new-dir (case node
                               :infected (turn-right dir)
                               :clean (turn-left dir)
                               :weakened dir
                               :flagged (mapv * dir [-1 -1]))]
                 (case node
                   :infected [new-dir (mapv + pos new-dir) infections (assoc grid pos :flagged)]
                   :clean [new-dir (mapv + pos new-dir) infections (assoc grid pos :weakened)]
                   :weakened [new-dir (mapv + pos new-dir) (inc infections) (assoc grid pos :infected)]
                   :flagged [new-dir (mapv + pos new-dir) infections (clean grid pos)])))
        print-state (fn [[dir pos _ grid tgrid]]
                      (prn dir)
                      (doseq [row (range -10 10)]
                        (prn)
                        (doseq [col (range -10 10)]
                          (print (if (contains? grid [row col])
                                   ({:infected \#
                                     :weakened \W
                                     :flagged \F} (grid [row col]))
                                   (if (= [row col] pos) \o \.)))))
                      (prn))]
    (loop [state initial-state bursts 0]
      (if (< bursts 10000000)
        (recur (step state) (inc bursts))
        (do (print-state state)
            (zipmap [:dir :pos :infections] state))))))

(defn day23-1 [input]
  (let [parse-instr (fn [line]
                      (let [[op a b] (s/split line #"\s+")]
                        (cond-> [(keyword op)
                                 (if (re-find #"\d+" a)
                                   (Long/parseLong a)
                                   (keyword a))]
                          b (conj (if (re-find #"\d+" b)
                                    (Long/parseLong b)
                                    (keyword b))))))
        program (mapv parse-instr (s/split-lines input))
        op-val (fn [state v] (if (number? v) v (get state v)))
        do-op (fn [state [op x y]]
                (let [state (case op
                              :set (assoc state x (op-val state y))
                              :sub (update state x - (op-val state y))
                              :mul (-> state (update x * (op-val state y)) (update :mul-invokations (fnil inc 0)))
                              :jnz (if (zero? (op-val state x))
                                     (update state :pc inc)
                                     (update state :pc + (op-val state y))))]
                  (cond-> state
                    (not= op :jnz) (update :pc inc))))
        initial-state (zipmap (into #{:pc} (comp
                                             (mapcat next)
                                             (filter keyword?)) program) (repeat 0))]
    (loop [state initial-state]
      (if-let [instruction (get program (:pc state))]
        (recur (do-op state instruction))
        state))))

(defn day23-2 [_input]
  ; enter program
  ; 0 -- 7 {:a 1, :b 109900, :c 126900, :d 0, :e 0, :f 0, :g 0, :h 0}
  ; 8  set :f 1
  ; 9  set :d 2
  ; 10 set :e 2
  ; 11 -- 19 inner loop - if (zero? ed-b) f = 0, e++,  leave when e == b
  ; 10 -- 23 outer loop - d++, if (d != b) to 10, leave when d == b
  ; 8 -- 31 outermost loop - if (f == 0) h++, if (b == c) exit!, b += 17, to 8

  ; The outermost loop continues as long as b != c. c never changes after inst 7, c = 126900
  ; b increases by 17 every loop, so it passes the f!=0 check 1001 times.
  ; The double inner loop sets f=0 if ed==b, for e in [0,b], d in [2, b).
  (let [state-after-7 {:a 1, :b 109900, :c 126900, :d 0, :e 0, :f 0, :g 0, :h 0}]
    (count
      (for [b (range (:b state-after-7) (+ (:c state-after-7) 17) 17)
            :when (some #(zero? (rem b %)) (range 2 b))]
        1))))

(defn group-with [f f2 coll]
  (persistent!
    (reduce
      (fn [ret x]
        (let [k (f x)]
          (assoc! ret k (conj (get ret k #{}) (f2 x)))))
      (transient {}) coll)))

(defn day24 [day input]
  (let [ports (group-with first peek (for [line (s/split-lines input)
                                           :let [[_ p1 p2] (re-find #"(\d+)/(\d+)" line)
                                                 [n1 n2] [(Long/parseLong p1) (Long/parseLong p2)]]
                                           [na nb] [[n1 n2] [n2 n1]]]
                                       [na [na nb]]))
        pop-port (fn [ports [p1 p2 :as port]]
                   (-> ports
                       (update (first port) disj port)
                       (update (second port) disj [p2 p1])))
        find-ports (fn [ports chain]
                     (if-let [last-port (peek chain)]
                       (get ports (peek last-port))
                       (get ports 0)))
        try-ports (fn try-ports [ports chain]
                    (let [next-ports (find-ports ports chain)]
                      (if (seq next-ports)
                        (mapcat #(try-ports (pop-port ports %) (conj chain %))
                                next-ports)
                        [chain])))
        score (fn [chain] (reduce + (flatten chain)))]
    (if (= day 1)
      (transduce (map score) max 0 (try-ports ports []))
      (let [chains (try-ports ports [])
            max-length (count (apply max-key count chains))]
        (transduce (comp
                     (filter (comp #{max-length} count))
                     (map score)) max 0 chains)))))

(defn- parse-turing-machine [input]
  (let [[first-part & parts] (into [] (comp
                                        (partition-by s/blank?)
                                        (filter (comp seq first))) (s/split-lines input))
        starting-state (keyword (second (re-find #"^Begin in state (\w)" (first first-part))))
        checksum-steps (Long/parseLong (second (re-find #"checksum after (\d+) steps.$" (second first-part))))
        parse-branch (fn [[write move goto]]
                       [(Long/parseLong (re-find #"\d" write))
                        (if (= "left" (re-find #"left|right" move)) -1 1)
                        (keyword (second (re-find #"Continue with state (\w)." goto)))])
        parse-if (fn [lines]
                   [(parse-branch (take 3 (drop 1 lines))) (parse-branch (take 3 (drop 5 lines)))])
        parse-part (fn [lines]
                     (let [for-state (keyword (second (re-find #"In state (\w):" (first lines))))]
                       [for-state (parse-if (next lines))]))]
    {:initial-state [#{} 0 starting-state]
     :states (into {} (map parse-part parts))
     :checksum-steps checksum-steps}))

(defn day25 [input]
  (let [value (fn [[tape cursor]] (if (contains? tape cursor) 1 0))
        write (fn [state v] (if (zero? v)
                              (update state 0 disj (second state))
                              (update state 0 conj (second state))))
        move-cursor (fn [state amount] (update state 1 + amount))
        goto (fn [state instr] (assoc state 2 instr))
        {:keys [initial-state states checksum-steps]} (parse-turing-machine input)
        do-branch (fn [state [wr mv go]]
                    (-> state (write wr) (move-cursor mv) (goto go)))
        turing-step (fn [[tape cursor instr :as state]]
                      (let [[br1 br2] (states instr)]
                        (do-branch state (if (zero? (value state)) br1 br2))))
        initial-tape #{}]
    (count (first (nth (iterate turing-step initial-state) checksum-steps)))))

(comment

  (day1-1 (read-resource "2017/input1.txt"))
  (day1-2 (read-resource "2017/input1.txt"))

  (day2-1 (read-resource "2017/input2.txt"))
  (day2-2 (read-resource "2017/input2.txt"))

  (day3-1 (read-resource "2017/input3.txt"))

  (day3-2 (read-resource "2017/input3.txt"))

  (day4-1 (read-resource "2017/input4.txt"))
  (day4-2 (read-resource "2017/input4.txt"))

  (day5-1 (read-resource "2017/input5.txt"))
  (day5-2 (read-resource "2017/input5.txt"))

  (day6-1 (read-resource "2017/input6.txt"))
  (day6-2 (read-resource "2017/input6.txt"))

  (day7-1 (read-resource "2017/input7.txt"))
  (day7-2 (read-resource "2017/input7.txt"))

  (day8 1 (read-resource "2017/input8.txt"))
  (day8 2 (read-resource "2017/input8.txt"))

  (day9 1 (read-resource "2017/input9.txt"))
  (day9 2 (read-resource "2017/input9.txt"))

  (day10-1 (read-resource "2017/input10.txt"))
  (day10-2 (read-resource "2017/input10.txt"))

  (day11 1 (read-resource "2017/input11.txt"))
  (day11 2 (read-resource "2017/input11.txt"))

  (day12 1 (read-resource "2017/input12.txt"))
  (day12 2 (read-resource "2017/input12.txt"))

  (day13-1 (read-resource "2017/input13.txt"))
  (day13-2 (read-resource "2017/input13.txt"))

  (day14-1 (read-resource "2017/input14.txt"))
  (day14-2 (read-resource "2017/input14.txt"))

  (day15 1 (read-resource "2017/input15.txt"))
  (day15 2 (read-resource "2017/input15.txt"))

  (day16 1 (read-resource "2017/input16.txt"))
  (day16 2 (read-resource "2017/input16.txt"))

  (day17-1 (read-resource "2017/input17.txt"))
  (day17-2 (read-resource "2017/input17.txt"))

  (day18 1 (read-resource "2017/input18.txt"))
  (day18 2 (read-resource "2017/input18.txt"))

  (day19 (read-resource "2017/input19.txt"))

  (day20 1 (read-resource "2017/input20.txt"))
  (day20 2 (read-resource "2017/input20.txt"))

  (day21 (read-resource "2017/input21.txt"))

  (day22-1 (read-resource "2017/input22.txt"))
  (day22-2 (read-resource "2017/input22.txt"))

  (day23-1 (read-resource "2017/input23.txt"))
  (day23-2 (read-resource "2017/input23.txt"))

  (day24 1 (read-resource "2017/input24.txt"))
  (day24 2 (read-resource "2017/input24.txt"))

  (day25 (read-resource "2017/input25.txt"))
  (parse-turing-machine (read-resource "2017/input25.txt"))

  )


