(ns adventofcode.util
  (:require [clojure.data.priority-map :as pm]
            [clojure.string :as s])
  (:import [java.security MessageDigest]
           [javax.xml.bind.annotation.adapters HexBinaryAdapter]))

(set! *warn-on-reflection* true)
;(set! *unchecked-math* :warn-on-boxed)

(defn read-input [year day]
  (slurp (str "resources/" year "/input" day ".txt")))

(defn parse-lines
  ([input] (parse-lines identity input))
  ([f input] (mapv f (s/split-lines input))))
(defn parse-nums [line] (mapv #(Long/parseLong (s/trim %)) (s/split line #"[\s+,]")))

(defn parse-grid
  ([input] (parse-grid identity input))
  ([f input]
   (into {}
         (mapcat
           (fn [[y l]]
             (into [] (map-indexed (fn [x ch]
                                     [[x y] (f ch)]) l)))) (map vector (range) (s/split-lines input)))))

(defn sum [xs] (reduce + xs))

(defn path [parents target]
  (reverse (take-while (complement #{:none}) (iterate parents target))))

(defn a*-search [starting-node target-node neighbours cost & [heuristic]]
  (let [frontier (pm/priority-map starting-node 0)
        came-from {starting-node :none}
        cost-so-far {starting-node 0}
        heuristic (or heuristic (constantly 0))]
    (loop [frontier frontier came-from came-from cost-so-far cost-so-far]
      (if-let [current (ffirst frontier)]
        (if (= current target-node)
          (path came-from target-node)
          (let [current-cost (cost-so-far current)
                neighs (neighbours current)
                [new-cost-so-far
                 new-frontier
                 new-came-from] (reduce (fn [[csf fr cf :as acc] next-node]
                                          (let [new-cost (+ current-cost (cost current next-node))]
                                            (if (or (not (contains? csf next-node))
                                                    (< new-cost (csf next-node)))
                                              [(assoc csf next-node new-cost)
                                               (conj fr [next-node (+ new-cost (heuristic target-node next-node))])
                                               (assoc cf next-node current)]
                                              acc)))
                                        [cost-so-far (pop frontier) came-from]
                                        neighs)]
            (recur new-frontier new-came-from new-cost-so-far)))
        :target-node-not-found))))

(defn bytes-to-hex [^bytes bytes]
  (s/lower-case (.marshal (HexBinaryAdapter.) bytes)))

(let [md5-inst (MessageDigest/getInstance "MD5")]
  (defn md5 [^String s] (bytes-to-hex (.digest md5-inst (.getBytes s)))))

(defn extended-gcd
  "The extended Euclidean algorithm
  Returns a list containing the GCD and the Bézout coefficients
  corresponding to the inputs."
  [a b]
  (cond (zero? a) [(Math/abs (long b)) 0 1]
        (zero? b) [(Math/abs (long a)) 1 0]
        :else (loop [s 0
                     s0 1
                     t 1
                     t0 0
                     r (Math/abs (long b))
                     r0 (Math/abs (long a))]
                (if (zero? r)
                  [r0 s0 t0]
                  (let [q (quot r0 r)]
                    (recur (- s0 (* q s)) s
                           (- t0 (* q t)) t
                           (- r0 (* q r)) r))))))

(defn chinese-remainder
  [n a]
  (let [prod (apply * n)
        reducer (fn [sum [n_i a_i]]
                  (let [p (quot prod n_i)
                        egcd (extended-gcd p n_i)
                        inv_p (second egcd)]
                    (+ sum (* a_i inv_p p))))
        sum-prod (reduce reducer 0 (map vector n a))]
    (mod sum-prod prod)))

(defn manhattan-distance [[x1 y1] [x2 y2]]
  (+ (Math/abs (long (- x2 x1))) (Math/abs (long (- y2 y1)))))